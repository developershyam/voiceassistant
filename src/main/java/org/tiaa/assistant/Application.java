
package org.tiaa.assistant;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Application {

  private static final Logger LOGGER = Logger.getLogger(Application.class.getName());

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
    // new SpringApplicationBuilder(Application.class).web(false).run(args);
  }
}
