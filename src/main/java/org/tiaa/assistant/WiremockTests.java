
package org.tiaa.assistant;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.junit.WireMockRule;

public class WiremockTests {

  RestTemplate restTemplate;
  WireMockServer wireMockServer;
  
  @Before
  public void before() {
    
    restTemplate = new RestTemplate();
    
    wireMockServer = new WireMockServer(8090);
    wireMockServer.start();
    configureFor("localhost", 8090);
  }
  
  @After
  public void after(){
    
    wireMockServer.stop();
  }
  
  @Test
  public void givenWireMockEndpoint_whenGetWithoutParams_thenVerifyRequest2() {
    stubFor(get(urlPathMatching("/users/.*"))
        .willReturn(aResponse().withStatus(HttpStatus.OK.value()).withHeader("Content-Type", TEXT_PLAIN_VALUE).withBody("test")));

    stubFor(get(urlMatching("/other/.*"))
        .willReturn(aResponse().proxiedFrom("http://jsonplaceholder.typicode.com")));
    
    ResponseEntity response = restTemplate.getForEntity("http://localhost:8090/other/1", String.class);

    System.out.println("========> " + response.getBody());
    System.out.println("========> " + response.getStatusCode().equals(HttpStatus.OK));
  }
}
