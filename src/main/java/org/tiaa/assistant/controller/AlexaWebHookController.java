
package org.tiaa.assistant.controller;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.tiaa.assistant.intent.IntentInterceptor;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.model.api.alexa.AlexaWebHookRequest;
import org.tiaa.assistant.model.api.alexa.AlexaWebHookResponse;
import org.tiaa.assistant.service.CacheService;
import org.tiaa.assistant.transform.AlexaTransformer;

import com.google.gson.Gson;

@RestController
public class AlexaWebHookController {

  @Autowired
  private IntentInterceptor intentInterceptor;

  @Autowired
  private AlexaTransformer alexaTransformer;

  @Autowired
  private CacheService cacheService;

  private static final Logger LOGGER = Logger.getLogger(AlexaWebHookController.class.getName());

  @PostMapping("/alexaWebHook")
  public AlexaWebHookResponse alexaWebHook(@RequestBody String input, HttpServletRequest request,
      HttpServletResponse response) {

    final long startTime = System.currentTimeMillis();
    LOGGER.info("======================================================================");
    LOGGER.info("Input: " + input.replaceAll(System.getProperty("line.separator"), ""));

    AlexaWebHookRequest alexaWebHookRequest = new Gson().fromJson(input, AlexaWebHookRequest.class);
    ;

    TIAAAssistantRequest tiaaAssistantRequest =
        alexaTransformer.getTIAAAssistantRequest(alexaWebHookRequest);
    TIAAAssistantResponse tiaaAssistantResponse = new TIAAAssistantResponse();
    LOGGER.info("Intent: " + tiaaAssistantRequest.getIntentName());

    // Get cache
    UserData userData = cacheService.getUserData(tiaaAssistantRequest.getSessionId());
    LOGGER.info("Pre UserData: " + userData);

    // Process Intent
    intentInterceptor.process(tiaaAssistantRequest, tiaaAssistantResponse, userData);

    // Update cache
    cacheService.updateUserData(tiaaAssistantRequest.getSessionId(), userData);
    cacheService.getActiveSession().put(tiaaAssistantRequest.getSessionId(),
        System.currentTimeMillis());

    AlexaWebHookResponse alexaWebHookResponse =
        alexaTransformer.getAlexaWebHookResponse(tiaaAssistantRequest, tiaaAssistantResponse);
    LOGGER.info("Post UserData: " + userData);

    long endTime = System.currentTimeMillis();
    LOGGER.info("Execution Millis: " + (endTime - startTime));
    LOGGER.info("AlexaWebHookResponse: " + new Gson().toJson(alexaWebHookResponse));
    LOGGER.info("***********************************************************************");
    return alexaWebHookResponse;
  }

}
