
package org.tiaa.assistant.controller;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.tiaa.assistant.intent.IntentInterceptor;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.model.api.google.GoogleWebHookRequest;
import org.tiaa.assistant.model.api.google.GoogleWebHookResponse;
import org.tiaa.assistant.service.CacheService;
import org.tiaa.assistant.transform.GoogleTransformer;
import org.tiaa.assistant.util.AppUtils;

import com.google.gson.Gson;

@RestController
public class GoogleWebHookController {

  @Autowired
  private IntentInterceptor intentInterceptor;

  @Autowired
  private GoogleTransformer googleTransformer;

  @Autowired
  private CacheService cacheService;

  private static final Logger LOGGER = Logger.getLogger(GoogleWebHookController.class.getName());

  /**
   * request.
   * 
   * @return json
   */
  @PostMapping("/googleWebHook")
  public GoogleWebHookResponse googleWebHook(@RequestBody String input, HttpServletRequest request,
      HttpServletResponse response) {

    final long startTime = System.currentTimeMillis();
    LOGGER.info("======================================================================");
    // LOGGER.info("Input: " + input.replaceAll(System.getProperty("line.separator"), ""));

    GoogleWebHookRequest googleWebHookRequest = AppUtils.parse(input, GoogleWebHookRequest.class);

    System.out.println("googleWebHookRequest: "+ AppUtils.getJson(googleWebHookRequest.getQueryResult()));
    TIAAAssistantRequest tiaaAssistantRequest =
        googleTransformer.getTIAAAssistantRequest(googleWebHookRequest);
    TIAAAssistantResponse tiaaAssistantResponse = new TIAAAssistantResponse();
    LOGGER.info("Intent: " + tiaaAssistantRequest.getIntentName());

    // Get cache
    UserData userData = cacheService.getUserData(tiaaAssistantRequest.getSessionId());
    LOGGER.info("Pre UserData: " + userData);

    // Process Intent
    intentInterceptor.process(tiaaAssistantRequest, tiaaAssistantResponse, userData);

    // Update cache
    cacheService.updateUserData(tiaaAssistantRequest.getSessionId(), userData);
    cacheService.getActiveSession().put(tiaaAssistantRequest.getSessionId(),
        System.currentTimeMillis());

    GoogleWebHookResponse googleWebHookResponse =
        googleTransformer.getGoogleWebHookResponse(tiaaAssistantRequest, tiaaAssistantResponse);
    LOGGER.info("Post UserData: " + userData);

    long endTime = System.currentTimeMillis();
    LOGGER.info("Execution Millis: " + (endTime - startTime));
    LOGGER.info("GoogleWebHookResponse: " + new Gson().toJson(googleWebHookResponse));
    LOGGER.info("***********************************************************************");
    return googleWebHookResponse;
  }
}
