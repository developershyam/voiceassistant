
package org.tiaa.assistant.controller;

import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.service.CacheService;

@RestController
public class TestAppEngineApplication {

  private static final Logger LOGGER = Logger.getLogger(TestAppEngineApplication.class.getName());

  @Autowired
  CacheService cacheService;

  /**
   * Test method.
   * 
   * @return str
   */
  @GetMapping("/test")
  public String test() {
    LOGGER.info("TestAppEngineApplication: test called.");
    return "Testing with Spring Boot!";
  }

  /**
   * get cache.
   * 
   * @return
   */
  @GetMapping("/getActiveSessions")
  public Map<String, Long> getActiveSessions() {
    LOGGER.info("TestAppEngineApplication: getActiveSessions");
    return cacheService.getActiveSession();
  }

  /**
   * get cache.
   * 
   * @param session
   *        id
   * @return
   */
  @GetMapping("/getCache/{session}")
  public UserData getCache(@PathVariable String session) {
    LOGGER.info("TestAppEngineApplication: getCache with session: " + session);
    cacheService.getActiveSession().put(session, System.currentTimeMillis());
    return cacheService.getUserData(session);
  }

  /**
   * Update cache.
   * 
   * @param session
   *        session id
   * @param goalName
   *        goal name
   * @param age
   *        age
   * @return
   */
  @GetMapping("/updateCache/{session}/{age}/{annualIncome}")
  public UserData updateCache(@PathVariable String session, @PathVariable Integer age, 
      @PathVariable Long annualIncome, @PathVariable Integer retirementAge) {
    LOGGER.info("TestAppEngineApplication: getCache with session: " + session);
    cacheService.getActiveSession().put(session, System.currentTimeMillis());
    cacheService.updateUserData(session, age, annualIncome, retirementAge);
    return cacheService.getUserData(session);
  }

}
