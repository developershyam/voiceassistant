
package org.tiaa.assistant.intent;

import static org.tiaa.assistant.util.AppConstants.PAUSE_800_MILLS;

import org.springframework.stereotype.Component;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.util.AppUtils;
import org.tiaa.assistant.util.SSMLBuilder;
import org.tiaa.assistant.util.WebHookUtils;

@Component
public class GoalStatusIntentHandler extends IntentHandler {

  public void handle(TIAAAssistantRequest tiaaAssistantRequest, TIAAAssistantResponse tiaaAssistantResponse, UserData userData) {

    String goalName = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "goalName", String.class);
    if (!AppUtils.hasValue(goalName)) {
      tiaaAssistantResponse.setAskForSlot("goalName");
      String responseSSML = SSMLBuilder.create().speak(getMessage("goalStatus.askForGoalName")).build();
      tiaaAssistantResponse.setResponseSpeech(responseSSML);
      return;
    }
    String actionForDetail = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "actionForDetail", String.class);
    if (!AppUtils.hasValue(actionForDetail)) {
      tiaaAssistantResponse.setAskForSlot("actionForDetail");
      String responseSSML = SSMLBuilder.create().speak(getMessage("goalStatus.info", 60000, "2030")).pause(PAUSE_800_MILLS)
          .speak(getMessage("goalStatus.askForMoreDetails", goalName)).build();
      tiaaAssistantResponse.setResponseSpeech(responseSSML);
    } else if (AppUtils.hasValue(actionForDetail) && actionForDetail.equalsIgnoreCase("YES")) {
      String responseSSML = SSMLBuilder.create().speak(getMessage("goalStatus.projectionWithGap", 43286, 16714, 195)).pause(PAUSE_800_MILLS)
          .speak(getMessage("goalStatus.suggestion.hint")).build();
      tiaaAssistantResponse.setResponseSpeech(responseSSML);
    } else {
      String responseSSML = SSMLBuilder.create().speak(getMessage("goalStatus.suggestion.hint")).build();
      tiaaAssistantResponse.setResponseSpeech(responseSSML);
    }

  }
}
