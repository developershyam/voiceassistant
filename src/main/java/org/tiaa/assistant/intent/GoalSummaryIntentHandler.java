
package org.tiaa.assistant.intent;

import static org.tiaa.assistant.util.AppConstants.PAUSE_400_MILLS;
import static org.tiaa.assistant.util.AppConstants.PAUSE_800_MILLS;

import org.springframework.stereotype.Component;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.util.SSMLBuilder;

@Component
public class GoalSummaryIntentHandler extends IntentHandler {

  public void handle(TIAAAssistantRequest tiaaAssistantRequest, TIAAAssistantResponse tiaaAssistantResponse, UserData userData) {

    String responseSSML = SSMLBuilder.create().speak(getMessage("goalSummary.goals", 2)).pause(PAUSE_400_MILLS)
        .speak(getMessage("goalSummary.goals.name", "Buy New Home", "Travel")).pause(PAUSE_400_MILLS).speak(getMessage("goalSummary.nba.retirement"))
        .pause(PAUSE_800_MILLS).speak(getMessage("goalSummary.suggestion.hint")).build();
    tiaaAssistantResponse.setResponseSpeech(responseSSML);
  }
}
