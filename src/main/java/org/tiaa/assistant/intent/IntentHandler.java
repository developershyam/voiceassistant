
package org.tiaa.assistant.intent;

import org.springframework.beans.factory.annotation.Autowired;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.service.RestService;
import org.tiaa.assistant.util.MessageUtility;

public abstract class IntentHandler {

  @Autowired
  protected MessageUtility messageUtility;
  
  @Autowired
  protected RestService restService;
  

  protected abstract void handle(TIAAAssistantRequest tiaaAssistantRequest, TIAAAssistantResponse tiaaAssistantResponse, UserData userData);

  protected String getMessage(String id) {
    return messageUtility.getMessage(id);
  }

  protected String getMessage(String id, Object... args) {
    return messageUtility.getMessage(id, args);
  }

}
