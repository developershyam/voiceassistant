
package org.tiaa.assistant.intent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.util.TIAAIntents;

@Component
public class IntentInterceptor {

  @Autowired
  private WelcomeIntentHandler welcomeIntentHandler;

  @Autowired
  private RetirementCheckupIntentHandler retirementCheckupIntentHandler;

  @Autowired
  private GoalSummaryIntentHandler goalSummaryIntentHandler;
  
  @Autowired
  private GoalStatusIntentHandler goalStatusIntentHandler;
  

  public void process(TIAAAssistantRequest tiaaAssistantRequest,
      TIAAAssistantResponse tiaaAssistantResponse, UserData userData) {

    if (TIAAIntents.DEFAULT_WELCOME_INTENT.equalsIgnoreCase(tiaaAssistantRequest.getIntentName())) {

      welcomeIntentHandler.handle(tiaaAssistantRequest, tiaaAssistantResponse, userData);
    }
    else if (TIAAIntents.GOAL_SUMMARY.equalsIgnoreCase(tiaaAssistantRequest.getIntentName())) {

      goalSummaryIntentHandler.handle(tiaaAssistantRequest, tiaaAssistantResponse, userData);
    }
    else if (TIAAIntents.RETIREMENT_CHECKUP
        .equalsIgnoreCase(tiaaAssistantRequest.getIntentName())) {

      retirementCheckupIntentHandler.handle(tiaaAssistantRequest, tiaaAssistantResponse, userData);
    }
    else if (TIAAIntents.GOAL_STATUS.equalsIgnoreCase(tiaaAssistantRequest.getIntentName())) {

      goalStatusIntentHandler.handle(tiaaAssistantRequest, tiaaAssistantResponse, userData);
    }
  }

}
