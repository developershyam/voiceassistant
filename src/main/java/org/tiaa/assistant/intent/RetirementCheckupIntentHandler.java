
package org.tiaa.assistant.intent;

import static org.tiaa.assistant.util.AppConstants.DONE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.util.AppUtils;
import org.tiaa.assistant.util.RetirementCheckupHelper;
import org.tiaa.assistant.util.SSMLBuilder;
import org.tiaa.assistant.util.WebHookUtils;

@Component
public class RetirementCheckupIntentHandler extends IntentHandler {

  @Autowired
  private RetirementCheckupHelper retirementCheckupHelper;

  @Override
  public void handle(TIAAAssistantRequest tiaaAssistantRequest, TIAAAssistantResponse tiaaAssistantResponse, UserData userData) {

    /************************ Edit Info **********************/
    String actionForEditInfo = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "actionForEditInfo", String.class);
    if (!AppUtils.hasValue(actionForEditInfo)) {
      // Ask for Edit or Continue
      String responseSSML = retirementCheckupHelper.getRetirementCheckupOverviewSSML(userData);

      tiaaAssistantResponse.setResponseSpeech(responseSSML);
      tiaaAssistantResponse.setAskForSlot("actionForEditInfo");
      return;
    } else if (AppUtils.hasValue(actionForEditInfo) && actionForEditInfo.equalsIgnoreCase("EDIT")) {

      // When user said Edit then ask for slots age, income, and retirement age.
      Integer age = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "age", Integer.class);
      Long annualIncome = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "annualIncome", Long.class);
      Integer retirementAge = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "retirementAge", Integer.class);
      // Skipping for asking age for now
      if (!AppUtils.hasValue(age)) {
        age = userData.getAge();
        tiaaAssistantRequest.getAttributes().put("age", userData.getAge());
      }

      if (!AppUtils.hasValue(age)) {
        tiaaAssistantResponse.setAskForSlot("age");
        tiaaAssistantRequest.getAttributes().put("age", "");
        String responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.askForAge")).build();
        tiaaAssistantResponse.setResponseSpeech(responseSSML);
      } else if (!AppUtils.hasValue(annualIncome)) {
        tiaaAssistantResponse.setAskForSlot("annualIncome");
        tiaaAssistantRequest.getAttributes().put("annualIncome", "");
        String responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.askForAnnualIncome")).build();
        tiaaAssistantResponse.setResponseSpeech(responseSSML);
      } else if (AppUtils.hasValue(annualIncome) && annualIncome < 10000) {
        tiaaAssistantResponse.setAskForSlot("annualIncome");
        tiaaAssistantRequest.getAttributes().put("annualIncome", "");
        String responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.askForAnnualIncome.error", 10000)).build();
        tiaaAssistantResponse.setResponseSpeech(responseSSML);
      } else if (!AppUtils.hasValue(retirementAge)) {
        tiaaAssistantResponse.setAskForSlot("retirementAge");
        tiaaAssistantRequest.getAttributes().put("retirementAge", "");
        String responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.askForRetirementAge")).build();
        tiaaAssistantResponse.setResponseSpeech(responseSSML);
      } else if (AppUtils.hasValue(retirementAge) && (retirementAge < age + 3 || retirementAge > 70)) {
        tiaaAssistantResponse.setAskForSlot("retirementAge");
        tiaaAssistantRequest.getAttributes().put("retirementAge", "");
        String responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.askForRetirementAge.error", 70)).build();
        tiaaAssistantResponse.setResponseSpeech(responseSSML);
      } else {

        // User said Edit then filled all the slots age, income, and retirement
        // age.
        // Make call for InvestmentAdvice and set replacement income.
        userData.setRetirementAge(retirementAge);
        userData.setAnnualIncome(annualIncome);

        String responseSSML = retirementCheckupHelper.getInvestmentAdviceSSML(userData, true);

        tiaaAssistantResponse.setResponseSpeech(responseSSML);
        tiaaAssistantRequest.getAttributes().put("actionForEditInfo", DONE);
        tiaaAssistantResponse.setAskForSlot("actionForOtherSavings");
      }
      return;
    } else if (AppUtils.hasValue(actionForEditInfo) && !actionForEditInfo.equalsIgnoreCase(DONE)) {
      // If user skipped or continue without Editing info. So fill slots with
      // default and move for next slot.
      tiaaAssistantRequest.getAttributes().put("age", userData.getAge());
      tiaaAssistantRequest.getAttributes().put("annualIncome", userData.getAnnualIncome());
      tiaaAssistantRequest.getAttributes().put("retirementAge", userData.getRetirementAge());
      tiaaAssistantRequest.getAttributes().put("actionForEditInfo", DONE);
      tiaaAssistantResponse.setAskForSlot("actionForOtherSavings");
    }
    /************************ Include Other Savings **********************/
    String actionForOtherSavings = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "actionForOtherSavings", String.class);
    if (!AppUtils.hasValue(actionForOtherSavings)) {
      // Ask for Other savings to include

      String responseSSML = retirementCheckupHelper.getInvestmentAdviceSSML(userData, false);

      tiaaAssistantResponse.setResponseSpeech(responseSSML);
      tiaaAssistantResponse.setAskForSlot("actionForOtherSavings");
      return;
    } else if (AppUtils.hasValue(actionForOtherSavings) && actionForOtherSavings.equalsIgnoreCase("YES")) {
      // User want to include so now need to fill all slots retirementSavings,
      // annualContribution, riskLevel
      Long retirementSavings = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "retirementSavings", Long.class);
      Long annualContribution = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "annualContribution", Long.class);
      String riskLevel = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "riskLevel", String.class);
      if (!AppUtils.hasValue(retirementSavings)) {
        tiaaAssistantResponse.setAskForSlot("retirementSavings");
        tiaaAssistantRequest.getAttributes().put("retirementSavings", "");
        String responseSSML = SSMLBuilder.create()
            .speak(getMessage("retirmentCheckup.askForRetirementSavings", SSMLBuilder.create().speakWithSayAs(getMessage("tiaa"), "characters").getAsString()))
            .build();
        tiaaAssistantResponse.setResponseSpeech(responseSSML);
      } else if (!AppUtils.hasValue(annualContribution)) {
        tiaaAssistantResponse.setAskForSlot("annualContribution");
        tiaaAssistantRequest.getAttributes().put("annualContribution", "");
        String responseSSML = SSMLBuilder.create()
            .speak(getMessage("retirmentCheckup.askForAnnualContribution", SSMLBuilder.create().speakWithSayAs(getMessage("tiaa"), "characters").getAsString()))
            .build();
        tiaaAssistantResponse.setResponseSpeech(responseSSML);
      } else if (!AppUtils.hasValue(riskLevel)) {
        tiaaAssistantResponse.setAskForSlot("riskLevel");
        tiaaAssistantRequest.getAttributes().put("riskLevel", "");
        String responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.askForRiskLevel")).build();
        tiaaAssistantResponse.setResponseSpeech(responseSSML);
      } else {
        // Once Other include savings slots filled.
        // Make call for retrieve projection.
        // Next step to ask for Action Plan
        userData.setRetirementSavings(retirementSavings);
        userData.setAnnualContribution(annualContribution);
        userData.setRiskLevel(riskLevel);

        String responseSSML = retirementCheckupHelper.getRetrieveProjectionSSML(userData);

        tiaaAssistantResponse.setResponseSpeech(responseSSML);
        tiaaAssistantRequest.getAttributes().put("actionForOtherSavings", DONE);
        tiaaAssistantResponse.setAskForSlot("actionForActionPlan");
        
        Long gap = userData.getReplacementIncome() - userData.getProjectedIncome();
        if(gap<=0) {
          tiaaAssistantResponse.setAskForSlot(null);
          tiaaAssistantRequest.getAttributes().put("actionForActionPlan", DONE);
          tiaaAssistantRequest.getAttributes().put("actionForActionPlan.original", DONE);
        }
      }
      return;
    } else if (AppUtils.hasValue(actionForOtherSavings) && !actionForOtherSavings.equalsIgnoreCase(DONE)) {

      // If user don't want to include outside savings, skip and fill slot with
      // default value.
      // Make call for retrieve projection.
      // Next step to ask for Action Plan

      tiaaAssistantRequest.getAttributes().put("retirementSavings", userData.getRetirementSavings());
      tiaaAssistantRequest.getAttributes().put("annualContribution", userData.getAnnualContribution());
      tiaaAssistantRequest.getAttributes().put("riskLevel", userData.getRiskLevel());

      String responseSSML = retirementCheckupHelper.getRetrieveProjectionSSML(userData);

      tiaaAssistantResponse.setResponseSpeech(responseSSML);
      tiaaAssistantRequest.getAttributes().put("actionForOtherSavings", DONE);
      tiaaAssistantResponse.setAskForSlot("actionForActionPlan");
      
      Long gap = userData.getReplacementIncome() - userData.getProjectedIncome();
      if(gap<=0) {
        tiaaAssistantResponse.setAskForSlot(null);
        tiaaAssistantRequest.getAttributes().put("actionForActionPlan", DONE);
        tiaaAssistantRequest.getAttributes().put("actionForActionPlan.original", DONE);
      }
      return;
    }
    /************************ Action Plan **********************/
    String actionForActionPlan = WebHookUtils.getParameterValue(tiaaAssistantRequest.getAttributes(), "actionForActionPlan", String.class);
    if (!AppUtils.hasValue(actionForActionPlan)) {
      // Ask for to take action for fill gap.
      // Make call for projection
      // Next step is Action Plan

      String responseSSML = retirementCheckupHelper.getRetrieveProjectionSSML(userData);

      tiaaAssistantResponse.setResponseSpeech(responseSSML);
      tiaaAssistantResponse.setAskForSlot("actionForActionPlan");
      
      Long gap = userData.getReplacementIncome() - userData.getProjectedIncome();
      if(gap<=0) {
        tiaaAssistantResponse.setAskForSlot(null);
        tiaaAssistantRequest.getAttributes().put("actionForActionPlan", DONE);
        tiaaAssistantRequest.getAttributes().put("actionForActionPlan.original", DONE);
      }
      return;
    }
    if (AppUtils.hasValue(actionForActionPlan) && actionForActionPlan.equalsIgnoreCase("YES")) {
      // When user ask for action plan to fill gap.
      // Make call for action advice to fill gap.

      String responseSSML = retirementCheckupHelper.getActionSSML(userData);

      tiaaAssistantResponse.setResponseSpeech(responseSSML);
      tiaaAssistantRequest.getAttributes().put("actionForActionPlan", DONE);
      return;
    } else {

      String responseSSML = retirementCheckupHelper.getRetirementCheckupDone(userData);

      tiaaAssistantResponse.setResponseSpeech(responseSSML);
      tiaaAssistantRequest.getAttributes().put("actionForActionPlan", DONE);
      tiaaAssistantRequest.getAttributes().put("actionForActionPlan.original", DONE);
    }

  }

}
