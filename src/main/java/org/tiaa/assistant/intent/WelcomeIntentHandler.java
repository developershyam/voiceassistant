
package org.tiaa.assistant.intent;

import static org.tiaa.assistant.util.AppConstants.PAUSE_800_MILLS;

import org.springframework.stereotype.Component;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.util.SSMLBuilder;

@Component
public class WelcomeIntentHandler extends IntentHandler {

  @Override
  public void handle(TIAAAssistantRequest tiaaAssistantRequest, TIAAAssistantResponse tiaaAssistantResponse, UserData userData) {

    String responseSSML =
        SSMLBuilder.create().speak(getMessage("welcome.content", SSMLBuilder.create().speakWithSayAs(getMessage("tiaa"), "characters").getAsString()))
            .pause(PAUSE_800_MILLS).speak(getMessage("welcome.content.hint.goalSummary")).build();
    tiaaAssistantResponse.setResponseSpeech(responseSSML);
  }

}
