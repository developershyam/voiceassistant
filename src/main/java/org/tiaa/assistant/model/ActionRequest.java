
package org.tiaa.assistant.model;

import java.util.ArrayList;
import java.util.List;

public class ActionRequest {

  private boolean hasExternalAccounts = false;
  private String erContribution = "Yes";
  private boolean particpantFreemium = true;
  private boolean includeSocialSecurity = true;

  private int userAge;
  private long grossAnnualIncome;
  private int retirementAge;

  private long retirementAssetBalance;
  private long contribution;
  private String riskLevel;

  private long replacementIncome;

  private List externalAccounts = new ArrayList();
  private long internalRetAcctBalance = 0;
  private long internalRetAcctCntrbn = 0;
  private String internalRetAcctRisk = "";

  public boolean isHasExternalAccounts() {
    return hasExternalAccounts;
  }

  public void setHasExternalAccounts(boolean hasExternalAccounts) {
    this.hasExternalAccounts = hasExternalAccounts;
  }

  public String getErContribution() {
    return erContribution;
  }

  public void setErContribution(String erContribution) {
    this.erContribution = erContribution;
  }

  public boolean isParticpantFreemium() {
    return particpantFreemium;
  }

  public void setParticpantFreemium(boolean particpantFreemium) {
    this.particpantFreemium = particpantFreemium;
  }

  public boolean isIncludeSocialSecurity() {
    return includeSocialSecurity;
  }

  public void setIncludeSocialSecurity(boolean includeSocialSecurity) {
    this.includeSocialSecurity = includeSocialSecurity;
  }

  public int getUserAge() {
    return userAge;
  }

  public void setUserAge(int userAge) {
    this.userAge = userAge;
  }

  public long getGrossAnnualIncome() {
    return grossAnnualIncome;
  }

  public void setGrossAnnualIncome(long grossAnnualIncome) {
    this.grossAnnualIncome = grossAnnualIncome;
  }

  public int getRetirementAge() {
    return retirementAge;
  }

  public void setRetirementAge(int retirementAge) {
    this.retirementAge = retirementAge;
  }

  public long getRetirementAssetBalance() {
    return retirementAssetBalance;
  }

  public void setRetirementAssetBalance(long retirementAssetBalance) {
    this.retirementAssetBalance = retirementAssetBalance;
  }

  public long getContribution() {
    return contribution;
  }

  public void setContribution(long contribution) {
    this.contribution = contribution;
  }

  public String getRiskLevel() {
    return riskLevel;
  }

  public void setRiskLevel(String riskLevel) {
    this.riskLevel = riskLevel;
  }

  public long getReplacementIncome() {
    return replacementIncome;
  }

  public void setReplacementIncome(long replacementIncome) {
    this.replacementIncome = replacementIncome;
  }

  public List getExternalAccounts() {
    return externalAccounts;
  }

  public void setExternalAccounts(List externalAccounts) {
    this.externalAccounts = externalAccounts;
  }

  public long getInternalRetAcctBalance() {
    return internalRetAcctBalance;
  }

  public void setInternalRetAcctBalance(long internalRetAcctBalance) {
    this.internalRetAcctBalance = internalRetAcctBalance;
  }

  public long getInternalRetAcctCntrbn() {
    return internalRetAcctCntrbn;
  }

  public void setInternalRetAcctCntrbn(long internalRetAcctCntrbn) {
    this.internalRetAcctCntrbn = internalRetAcctCntrbn;
  }

  public String getInternalRetAcctRisk() {
    return internalRetAcctRisk;
  }

  public void setInternalRetAcctRisk(String internalRetAcctRisk) {
    this.internalRetAcctRisk = internalRetAcctRisk;
  }
}
