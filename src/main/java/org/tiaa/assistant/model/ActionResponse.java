
package org.tiaa.assistant.model;

public class ActionResponse {

  private long currentAmount;
  private long differenceAmount;
  private long engineRecommendedAmount;
  private boolean errorOccured;
  private String projectionsGUID;
  private long proposedAmount;
  private String recommendationStatus;
  private boolean showEmployerRetirementPlans;
  private boolean showIRTLink;
  private String status;

  public long getCurrentAmount() {
    return currentAmount;
  }

  public void setCurrentAmount(long currentAmount) {
    this.currentAmount = currentAmount;
  }

  public long getDifferenceAmount() {
    return differenceAmount;
  }

  public void setDifferenceAmount(long differenceAmount) {
    this.differenceAmount = differenceAmount;
  }

  public long getEngineRecommendedAmount() {
    return engineRecommendedAmount;
  }

  public void setEngineRecommendedAmount(long engineRecommendedAmount) {
    this.engineRecommendedAmount = engineRecommendedAmount;
  }

  public boolean isErrorOccured() {
    return errorOccured;
  }

  public void setErrorOccured(boolean errorOccured) {
    this.errorOccured = errorOccured;
  }

  public String getProjectionsGUID() {
    return projectionsGUID;
  }

  public void setProjectionsGUID(String projectionsGUID) {
    this.projectionsGUID = projectionsGUID;
  }

  public long getProposedAmount() {
    return proposedAmount;
  }

  public void setProposedAmount(long proposedAmount) {
    this.proposedAmount = proposedAmount;
  }

  public String getRecommendationStatus() {
    return recommendationStatus;
  }

  public void setRecommendationStatus(String recommendationStatus) {
    this.recommendationStatus = recommendationStatus;
  }

  public boolean isShowEmployerRetirementPlans() {
    return showEmployerRetirementPlans;
  }

  public void setShowEmployerRetirementPlans(boolean showEmployerRetirementPlans) {
    this.showEmployerRetirementPlans = showEmployerRetirementPlans;
  }

  public boolean isShowIRTLink() {
    return showIRTLink;
  }

  public void setShowIRTLink(boolean showIRTLink) {
    this.showIRTLink = showIRTLink;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "ActionResponse [currentAmount=" + currentAmount + ", differenceAmount=" + differenceAmount + ", engineRecommendedAmount=" + engineRecommendedAmount
        + ", errorOccured=" + errorOccured + ", projectionsGUID=" + projectionsGUID + ", proposedAmount=" + proposedAmount + ", recommendationStatus="
        + recommendationStatus + ", showEmployerRetirementPlans=" + showEmployerRetirementPlans + ", showIRTLink=" + showIRTLink + ", status=" + status + "]";
  }

}
