
package org.tiaa.assistant.model;

public class IncomeProjectionsModal {

  private boolean errorOccured;
  private String projectionsGUID;

  private Object errorMessages;

  private Object incomeProjections;

  public boolean isErrorOccured() {
    return errorOccured;
  }

  public void setErrorOccured(boolean errorOccured) {
    this.errorOccured = errorOccured;
  }

  public String getProjectionsGUID() {
    return projectionsGUID;
  }

  public void setProjectionsGUID(String projectionsGUID) {
    this.projectionsGUID = projectionsGUID;
  }

  public Object getErrorMessages() {
    return errorMessages;
  }

  public void setErrorMessages(Object errorMessages) {
    this.errorMessages = errorMessages;
  }

  public Object getIncomeProjections() {
    return incomeProjections;
  }

  public void setIncomeProjections(Object incomeProjections) {
    this.incomeProjections = incomeProjections;
  }

}
