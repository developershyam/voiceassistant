package org.tiaa.assistant.model;

public class InvestmentAdvice {

  private Boolean errorOccured;
  
  private String status;
  
  private RetrieveIncomeGuidance retrieveIncomeGuidance;

  public RetrieveIncomeGuidance getRetrieveIncomeGuidance() {
    return retrieveIncomeGuidance;
  }

  public void setRetrieveIncomeGuidance(RetrieveIncomeGuidance retrieveIncomeGuidance) {
    this.retrieveIncomeGuidance = retrieveIncomeGuidance;
  }

  public Boolean getErrorOccured() {
    return errorOccured;
  }

  public void setErrorOccured(Boolean errorOccured) {
    this.errorOccured = errorOccured;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "InvestmentAdvice [errorOccured=" + errorOccured + ", status=" + status + ", retrieveIncomeGuidance=" + retrieveIncomeGuidance + "]";
  }
  
}
