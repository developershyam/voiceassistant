package org.tiaa.assistant.model;

public class InvestmentProfile {

  public InvestmentProfile() {
    // TODO Auto-generated constructor stub
  }
  
  public InvestmentProfile(int userAge, long grossAnnualIncome) {
    this.userAge = userAge;
    this.grossAnnualIncome = grossAnnualIncome;
  }
  
  private int userAge; 
  
  private long grossAnnualIncome;

  public int getUserAge() {
    return userAge;
  }

  public void setUserAge(int userAge) {
    this.userAge = userAge;
  }

  public long getGrossAnnualIncome() {
    return grossAnnualIncome;
  }

  public void setGrossAnnualIncome(long grossAnnualIncome) {
    this.grossAnnualIncome = grossAnnualIncome;
  }

  @Override
  public String toString() {
    return "InvestmentProfile [userAge=" + userAge + ", grossAnnualIncome=" + grossAnnualIncome + "]";
  }
  
  
}
