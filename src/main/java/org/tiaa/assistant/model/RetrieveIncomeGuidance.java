package org.tiaa.assistant.model;

public class RetrieveIncomeGuidance {
  
  private int afterTaxRetirementIncomeGoal;
  
  private int lifeExpectancyAge;
  
  private String overAllRiskLevel;
  
  private int socialSecurityIncome;

  public int getAfterTaxRetirementIncomeGoal() {
    return afterTaxRetirementIncomeGoal;
  }

  public void setAfterTaxRetirementIncomeGoal(int afterTaxRetirementIncomeGoal) {
    this.afterTaxRetirementIncomeGoal = afterTaxRetirementIncomeGoal;
  }

  public int getLifeExpectancyAge() {
    return lifeExpectancyAge;
  }

  public void setLifeExpectancyAge(int lifeExpectancyAge) {
    this.lifeExpectancyAge = lifeExpectancyAge;
  }

  public String getOverAllRiskLevel() {
    return overAllRiskLevel;
  }

  public void setOverAllRiskLevel(String overAllRiskLevel) {
    this.overAllRiskLevel = overAllRiskLevel;
  }

  public int getSocialSecurityIncome() {
    return socialSecurityIncome;
  }
  
  public void setSocialSecurityIncome(int socialSecurityIncome) {
    this.socialSecurityIncome = socialSecurityIncome;
  }
}
