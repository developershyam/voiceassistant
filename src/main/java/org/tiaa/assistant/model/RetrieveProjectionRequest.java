
package org.tiaa.assistant.model;

public class RetrieveProjectionRequest {

  private boolean hasExternalAccounts = false;
  private String erContribution = "Yes";
  private boolean particpantFreemium = true;
  private boolean includeSocialSecurity = true;

  private int userAge;
  private long grossAnnualIncome;
  private int retirementAge;

  private long retirementAssetBalance;
  private long contribution;
  private String riskLevel;

  private long replacementIncome;

  public boolean isHasExternalAccounts() {
    return hasExternalAccounts;
  }

  public void setHasExternalAccounts(boolean hasExternalAccounts) {
    this.hasExternalAccounts = hasExternalAccounts;
  }

  public String getErContribution() {
    return erContribution;
  }

  public void setErContribution(String erContribution) {
    this.erContribution = erContribution;
  }

  public boolean isParticpantFreemium() {
    return particpantFreemium;
  }

  public void setParticpantFreemium(boolean particpantFreemium) {
    this.particpantFreemium = particpantFreemium;
  }

  public boolean isIncludeSocialSecurity() {
    return includeSocialSecurity;
  }

  public void setIncludeSocialSecurity(boolean includeSocialSecurity) {
    this.includeSocialSecurity = includeSocialSecurity;
  }

  public int getUserAge() {
    return userAge;
  }

  public void setUserAge(int userAge) {
    this.userAge = userAge;
  }

  public long getGrossAnnualIncome() {
    return grossAnnualIncome;
  }

  public void setGrossAnnualIncome(long grossAnnualIncome) {
    this.grossAnnualIncome = grossAnnualIncome;
  }

  public int getRetirementAge() {
    return retirementAge;
  }

  public void setRetirementAge(int retirementAge) {
    this.retirementAge = retirementAge;
  }

  public long getRetirementAssetBalance() {
    return retirementAssetBalance;
  }

  public void setRetirementAssetBalance(long retirementAssetBalance) {
    this.retirementAssetBalance = retirementAssetBalance;
  }

  public long getContribution() {
    return contribution;
  }

  public void setContribution(long contribution) {
    this.contribution = contribution;
  }

  public String getRiskLevel() {
    return riskLevel;
  }

  public void setRiskLevel(String riskLevel) {
    this.riskLevel = riskLevel;
  }

  public long getReplacementIncome() {
    return replacementIncome;
  }

  public void setReplacementIncome(long replacementIncome) {
    this.replacementIncome = replacementIncome;
  }

  @Override
  public String toString() {
    return "RetrieveProjectionRequest [hasExternalAccounts=" + hasExternalAccounts + ", erContribution=" + erContribution + ", particpantFreemium="
        + particpantFreemium + ", includeSocialSecurity=" + includeSocialSecurity + ", userAge=" + userAge + ", grossAnnualIncome=" + grossAnnualIncome
        + ", retirementAge=" + retirementAge + ", retirementAssetBalance=" + retirementAssetBalance + ", contribution=" + contribution + ", riskLevel="
        + riskLevel + ", replacementIncome=" + replacementIncome + "]";
  }
  
  
}
