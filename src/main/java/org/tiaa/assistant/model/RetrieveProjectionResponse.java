
package org.tiaa.assistant.model;

import java.math.BigDecimal;
import java.util.Map;

public class RetrieveProjectionResponse {

  private boolean errorOccured;
  private String status;
  private String trackingStatus;
  private IncomeProjectionsModal incomeProjectionsModal;
  // private RetrievedProjection retrievedProjections;
  private Map<String, BigDecimal> retrievedProjections;

  public boolean isErrorOccured() {
    return errorOccured;
  }

  public void setErrorOccured(boolean errorOccured) {
    this.errorOccured = errorOccured;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getTrackingStatus() {
    return trackingStatus;
  }

  public void setTrackingStatus(String trackingStatus) {
    this.trackingStatus = trackingStatus;
  }

  public IncomeProjectionsModal getIncomeProjectionsModal() {
    return incomeProjectionsModal;
  }

  public void setIncomeProjectionsModal(IncomeProjectionsModal incomeProjectionsModal) {
    this.incomeProjectionsModal = incomeProjectionsModal;
  }

  public Map<String, BigDecimal> getRetrievedProjections() {
    return retrievedProjections;
  }

  public void setRetrievedProjections(Map<String, BigDecimal> retrievedProjections) {
    this.retrievedProjections = retrievedProjections;
  }

  @Override
  public String toString() {
    return "RetrieveProjectionResponse [errorOccured=" + errorOccured + ", status=" + status + ", trackingStatus=" + trackingStatus
        + ", incomeProjectionsModal=" + incomeProjectionsModal + ", retrievedProjections=" + retrievedProjections + "]";
  }

}
