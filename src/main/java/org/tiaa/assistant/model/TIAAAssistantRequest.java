
package org.tiaa.assistant.model;

import java.util.LinkedHashMap;
import java.util.Map;

import org.tiaa.assistant.model.api.alexa.Intent;
import org.tiaa.assistant.model.api.google.QueryResult;

public class TIAAAssistantRequest {

  private String deviceName;

  private String version;

  private String sessionId;

  private String intentName;

  private Map<String, Object> attributes = new LinkedHashMap<>();

  private QueryResult queryResult;

  private Intent intent;

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getIntentName() {
    return intentName;
  }

  public void setIntentName(String intentName) {
    this.intentName = intentName;
  }

  public Map<String, Object> getAttributes() {
    return attributes;
  }

  public void setAttributes(Map<String, Object> attributes) {
    this.attributes = attributes;
  }

  public QueryResult getQueryResult() {
    return queryResult;
  }

  public void setQueryResult(QueryResult queryResult) {
    this.queryResult = queryResult;
  }

  public String getDeviceName() {
    return deviceName;
  }

  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }

  public Intent getIntent() {
    return intent;
  }

  public void setIntent(Intent intent) {
    this.intent = intent;
  }

}
