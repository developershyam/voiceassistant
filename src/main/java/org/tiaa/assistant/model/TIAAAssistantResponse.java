package org.tiaa.assistant.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class TIAAAssistantResponse {

  private String responseType = "SSML";
  
  private String responseSpeech;
  
  private String displayText;
  
  private String askForSlot;
  
  public String getResponseType() {
    return responseType;
  }

  public void setResponseType(String responseType) {
    this.responseType = responseType;
  }

  public String getResponseSpeech() {
    return responseSpeech;
  }

  public void setResponseSpeech(String responseSpeech) {
    this.responseSpeech = responseSpeech;
  }
  
  public String getDisplayText() {
    return displayText;
  }
  
  public void setDisplayText(String displayText) {
    this.displayText = displayText;
  }
  
  public void setAskForSlot(String askForSlot) {
    this.askForSlot = askForSlot;
  }
  
  public String getAskForSlot() {
    return askForSlot;
  }
}