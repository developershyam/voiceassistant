
package org.tiaa.assistant.model;

public class UserData {

  private String session;
  private Integer age = 32;
  private Long annualIncome = 80000l;
  private Integer retirementAge = 67;
  private Long retirementSavings = 0l;
  private Long annualContribution = 0l;
  private String riskLevel = "Moderate";
  public Long replacementIncome;
  public Long actionDifferenceAmount;
  public String trackingStatus;
  public Long projectedIncome;
  

  public UserData() {
    // TODO Auto-generated constructor stub
  }
  

  /**
   * Constructor for populate obj with defined values.
   * @param session user session id.
   * @param goalName goal name.
   * @param age current age.
   */
  public UserData(String session, Integer age, Long annualIncome, Integer retirementAge) {
    this.session = session;
    this.age = age;
    this.annualIncome = annualIncome;
    this.retirementAge = retirementAge;
  }

  public String getSession() {
    return session;
  }

  public void setSession(String session) {
    this.session = session;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }
  
  public Long getAnnualIncome() {
    return annualIncome;
  }
  
  public void setAnnualIncome(Long annualIncome) {
    this.annualIncome = annualIncome;
  }
  
  public Integer getRetirementAge() {
    return retirementAge;
  }
  
  public void setRetirementAge(Integer retirementAge) {
    this.retirementAge = retirementAge;
  }


  public Long getRetirementSavings() {
    return retirementSavings;
  }


  public void setRetirementSavings(Long retirementSavings) {
    this.retirementSavings = retirementSavings;
  }


  public Long getAnnualContribution() {
    return annualContribution;
  }


  public void setAnnualContribution(Long annualContribution) {
    this.annualContribution = annualContribution;
  }


  public String getRiskLevel() {
    return riskLevel;
  }


  public void setRiskLevel(String riskLevel) {
    this.riskLevel = riskLevel;
  }


  public Long getReplacementIncome() {
    return replacementIncome;
  }


  public void setReplacementIncome(Long replacementIncome) {
    this.replacementIncome = replacementIncome;
  }


  public Long getProjectedIncome() {
    return projectedIncome;
  }


  public void setProjectedIncome(Long projectedIncome) {
    this.projectedIncome = projectedIncome;
  }
  
  public String getTrackingStatus() {
    return trackingStatus;
  }
  
  public void setTrackingStatus(String trackingStatus) {
    this.trackingStatus = trackingStatus;
  }
  
  public Long getActionDifferenceAmount() {
    return actionDifferenceAmount;
  }
  
  public void setActionDifferenceAmount(Long actionDifferenceAmount) {
    this.actionDifferenceAmount = actionDifferenceAmount;
  }


  @Override
  public String toString() {
    return "UserData [session=" + session + ", age=" + age + ", annualIncome=" + annualIncome + ", retirementAge=" + retirementAge + ", retirementSavings="
        + retirementSavings + ", annualContribution=" + annualContribution + ", riskLevel=" + riskLevel + ", replacementIncome=" + replacementIncome
        + ", actionDifferenceAmount=" + actionDifferenceAmount + ", trackingStatus=" + trackingStatus + ", projectedIncome=" + projectedIncome + "]";
  }

  
}
