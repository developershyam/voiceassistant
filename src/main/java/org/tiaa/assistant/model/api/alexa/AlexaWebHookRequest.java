package org.tiaa.assistant.model.api.alexa;

public class AlexaWebHookRequest {

  private String version;
  
  private Session session;
  
  private Context context;
  
  private Request request;

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public Session getSession() {
    return session;
  }

  public void setSession(Session session) {
    this.session = session;
  }

  public Context getContext() {
    return context;
  }

  public void setContext(Context context) {
    this.context = context;
  }

  public Request getRequest() {
    return request;
  }

  public void setRequest(Request request) {
    this.request = request;
  }
  
  
}
