package org.tiaa.assistant.model.api.alexa;

import java.util.Map;

public class AlexaWebHookResponse {

  private String version;
  
  private Map<String, Object> sessionAttributes;
  
  private Response response;
  
  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public Map<String, Object> getSessionAttributes() {
    return sessionAttributes;
  }

  public void setSessionAttributes(Map<String, Object> sessionAttributes) {
    this.sessionAttributes = sessionAttributes;
  }

  public Response getResponse() {
    return response;
  }

  public void setResponse(Response response) {
    this.response = response;
  }
  
}
