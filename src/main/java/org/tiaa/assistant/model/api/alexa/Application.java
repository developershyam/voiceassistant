package org.tiaa.assistant.model.api.alexa;

public class Application {
  
  private String applicationId;

  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }
  
  

}
