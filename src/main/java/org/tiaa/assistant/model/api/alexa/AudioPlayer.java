package org.tiaa.assistant.model.api.alexa;

public class AudioPlayer {

  private String accessToken;
  
  private String token;
  
  private String offsetInMilliseconds;

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getOffsetInMilliseconds() {
    return offsetInMilliseconds;
  }

  public void setOffsetInMilliseconds(String offsetInMilliseconds) {
    this.offsetInMilliseconds = offsetInMilliseconds;
  }
  
  
}
