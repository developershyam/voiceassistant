package org.tiaa.assistant.model.api.alexa;

public class Context {

  private System system;
  
  private AudioPlayer AudioPlayer;

  public System getSystem() {
    return system;
  }

  public void setSystem(System system) {
    this.system = system;
  }

  public AudioPlayer getAudioPlayer() {
    return AudioPlayer;
  }

  public void setAudioPlayer(AudioPlayer audioPlayer) {
    AudioPlayer = audioPlayer;
  }
  
  
}
