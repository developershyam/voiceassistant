package org.tiaa.assistant.model.api.alexa;

public class Device {

  private String deviceId;
  
  private SupportedInterfaces supportedInterfaces;

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public SupportedInterfaces getSupportedInterfaces() {
    return supportedInterfaces;
  }

  public void setSupportedInterfaces(SupportedInterfaces supportedInterfaces) {
    this.supportedInterfaces = supportedInterfaces;
  }
  
  
  
}
