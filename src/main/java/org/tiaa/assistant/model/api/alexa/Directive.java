
package org.tiaa.assistant.model.api.alexa;

public class Directive {

  public Directive() {
    // TODO Auto-generated constructor stub
  }

  public Directive(String type, String slotToElicit) {

    this.type = type;
    this.slotToElicit = slotToElicit;
  }

  private String type;

  private String slotToElicit;
  
  private Intent updatedIntent;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getSlotToElicit() {
    return slotToElicit;
  }

  public void setSlotToElicit(String slotToElicit) {
    this.slotToElicit = slotToElicit;
  }

  public Intent getUpdatedIntent() {
    return updatedIntent;
  }
  
  public void setUpdatedIntent(Intent updatedIntent) {
    this.updatedIntent = updatedIntent;
  }
}
