package org.tiaa.assistant.model.api.alexa;

public class Image {
  
  private String smallImageUrl;
  
  private String largeImageUrl;

  public String getSmallImageUrl() {
    return smallImageUrl;
  }

  public void setSmallImageUrl(String smallImageUrl) {
    this.smallImageUrl = smallImageUrl;
  }

  public String getLargeImageUrl() {
    return largeImageUrl;
  }

  public void setLargeImageUrl(String largeImageUrl) {
    this.largeImageUrl = largeImageUrl;
  }
  
  

}
