package org.tiaa.assistant.model.api.alexa;

import java.util.Map;

public class Intent {

  private String name;
  
  private String confirmationStatus;
  
  private Map<String, Slot> slots;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getConfirmationStatus() {
    return confirmationStatus;
  }

  public void setConfirmationStatus(String confirmationStatus) {
    this.confirmationStatus = confirmationStatus;
  }

  public Map<String, Slot> getSlots() {
    return slots;
  }

  public void setSlots(Map<String, Slot> slots) {
    this.slots = slots;
  }
  
}
