package org.tiaa.assistant.model.api.alexa;

public class Request {

  private String locale;
  
  private String type;
  
  private String requestId;
  
  private String timestamp;
  
  private Intent intent;
  
  private String dialogState;
  
  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public Intent getIntent() {
    return intent;
  }

  public void setIntent(Intent intent) {
    this.intent = intent;
  }

  public String getDialogState() {
    return dialogState;
  }

  public void setDialogState(String dialogState) {
    this.dialogState = dialogState;
  }
  
}
