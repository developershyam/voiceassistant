package org.tiaa.assistant.model.api.alexa;

import java.util.List;

public class Response {

  private OutputSpeech outputSpeech;
  
  private Card card;
  
  private Reprompt reprompt;
  
  private List<Directive> directives;
  
  private Boolean shouldEndSession = false;

  public OutputSpeech getOutputSpeech() {
    return outputSpeech;
  }

  public void setOutputSpeech(OutputSpeech outputSpeech) {
    this.outputSpeech = outputSpeech;
  }

  public Card getCard() {
    return card;
  }

  public void setCard(Card card) {
    this.card = card;
  }

  public Reprompt getReprompt() {
    return reprompt;
  }

  public void setReprompt(Reprompt reprompt) {
    this.reprompt = reprompt;
  }

  public List<Directive> getDirectives() {
    return directives;
  }

  public void setDirectives(List<Directive> directives) {
    this.directives = directives;
  }

  public Boolean getShouldEndSession() {
    return shouldEndSession;
  }

  public void setShouldEndSession(Boolean shouldEndSession) {
    this.shouldEndSession = shouldEndSession;
  }
  
}
