package org.tiaa.assistant.model.api.alexa;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Session {

  @JsonProperty("new")
  private Boolean newSession;
  
  private String sessionId;
  
  private Application application;
  
  private Map<String, Object> attributes;
  
  private User user;

  public Boolean getNewSession() {
    return newSession;
  }

  public void setNewSession(Boolean newSession) {
    this.newSession = newSession;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public Application getApplication() {
    return application;
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public Map<String, Object> getAttributes() {
    return attributes;
  }

  public void setAttributes(Map<String, Object> attributes) {
    this.attributes = attributes;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
  
}
