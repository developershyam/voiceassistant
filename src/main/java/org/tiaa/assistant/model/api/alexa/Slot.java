
package org.tiaa.assistant.model.api.alexa;

public class Slot {

  private String name;
  
  private String value;
  
  private String source;
  
  private String confirmationStatus;
  
  public Slot() {
    // TODO Auto-generated constructor stub
  }
  
  public Slot(String name, String value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }
  
  public String getConfirmationStatus() {
    return confirmationStatus;
  }
  
  public void setConfirmationStatus(String confirmationStatus) {
    this.confirmationStatus = confirmationStatus;
  }
  

}
