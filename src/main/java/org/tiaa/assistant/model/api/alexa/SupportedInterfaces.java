package org.tiaa.assistant.model.api.alexa;

public class SupportedInterfaces {

  private AudioPlayer AudioPlayer;

  public AudioPlayer getAudioPlayer() {
    return AudioPlayer;
  }

  public void setAudioPlayer(AudioPlayer audioPlayer) {
    AudioPlayer = audioPlayer;
  }
}
