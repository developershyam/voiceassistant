package org.tiaa.assistant.model.api.alexa;

public class System {

  private Device device;
  
  private Application application;
  
  private User user;
  
  private String apiEndpoint;
  
  private String apiAccessToken;

  public Device getDevice() {
    return device;
  }

  public void setDevice(Device device) {
    this.device = device;
  }

  public Application getApplication() {
    return application;
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getApiEndpoint() {
    return apiEndpoint;
  }

  public void setApiEndpoint(String apiEndpoint) {
    this.apiEndpoint = apiEndpoint;
  }

  public String getApiAccessToken() {
    return apiAccessToken;
  }

  public void setApiAccessToken(String apiAccessToken) {
    this.apiAccessToken = apiAccessToken;
  }
  
  
}
