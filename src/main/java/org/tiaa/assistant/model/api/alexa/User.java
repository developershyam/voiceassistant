package org.tiaa.assistant.model.api.alexa;

import java.util.Map;

public class User {

  private String userId;
  
  private String applicationId;
  
  private String accessToken;
  
  private Map<String, Object> permissions;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public Map<String, Object> getPermissions() {
    return permissions;
  }

  public void setPermissions(Map<String, Object> permissions) {
    this.permissions = permissions;
  }
  
}
