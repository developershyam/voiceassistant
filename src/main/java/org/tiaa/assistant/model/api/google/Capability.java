package org.tiaa.assistant.model.api.google;

public class Capability {

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  
}
