
package org.tiaa.assistant.model.api.google;

public class Conversation {

  private String conversationId;
  private String type;

  private String conversationToken;

  public String getConversationId() {
    return conversationId;
  }

  public void setConversationId(String conversationId) {
    this.conversationId = conversationId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getConversationToken() {
    return conversationToken;
  }

  public void setConversationToken(String conversationToken) {
    this.conversationToken = conversationToken;
  }

}
