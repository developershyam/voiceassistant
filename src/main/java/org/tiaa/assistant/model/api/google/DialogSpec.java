package org.tiaa.assistant.model.api.google;

public class DialogSpec {
  
  private String requestConfirmationText;

  public String getRequestConfirmationText() {
    return requestConfirmationText;
  }

  public void setRequestConfirmationText(String requestConfirmationText) {
    this.requestConfirmationText = requestConfirmationText;
  }
  
  

}
