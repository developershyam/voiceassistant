package org.tiaa.assistant.model.api.google;

import java.util.Map;

public class FollowupEventInput {

  private String name;
  
  private String languageCode;
  
  private Map<String, Object> parameters;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLanguageCode() {
    return languageCode;
  }

  public void setLanguageCode(String languageCode) {
    this.languageCode = languageCode;
  }

  public Map<String, Object> getParameters() {
    return parameters;
  }

  public void setParameters(Map<String, Object> parameters) {
    this.parameters = parameters;
  }
  
  
  
}
