package org.tiaa.assistant.model.api.google;

public class FulfillmentMessage {

  private Text text;
  
  private Text card;

  public Text getText() {
    return text;
  }

  public void setText(Text text) {
    this.text = text;
  }

  public Text getCard() {
    return card;
  }

  public void setCard(Text card) {
    this.card = card;
  }
  
  
}
