package org.tiaa.assistant.model.api.google;

public class Google {
  
  private Boolean expectUserResponse;
  
  private RichResponse richResponse;

  public Boolean getExpectUserResponse() {
    return expectUserResponse;
  }

  public void setExpectUserResponse(Boolean expectUserResponse) {
    this.expectUserResponse = expectUserResponse;
  }

  public RichResponse getRichResponse() {
    return richResponse;
  }

  public void setRichResponse(RichResponse richResponse) {
    this.richResponse = richResponse;
  }
  
  
  
}
