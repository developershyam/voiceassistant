package org.tiaa.assistant.model.api.google;

/**
 * https://dialogflow.com/docs/fulfillment/how-it-works
 * https://github.com/dialogflow/fulfillment-webhook-json/tree/master/requests/v2
 * 
 * @author Admin
 *
 */
public class GoogleWebHookRequest {

  private String responseId;
  
  private String session;
  
  private QueryResult queryResult;
  
  private OriginalDetectIntentRequest originalDetectIntentRequest;

  public String getResponseId() {
    return responseId;
  }

  public void setResponseId(String responseId) {
    this.responseId = responseId;
  }

  public String getSession() {
    return session;
  }

  public void setSession(String session) {
    this.session = session;
  }

  public QueryResult getQueryResult() {
    return queryResult;
  }

  public void setQueryResult(QueryResult queryResult) {
    this.queryResult = queryResult;
  }

  public OriginalDetectIntentRequest getOriginalDetectIntentRequest() {
    return originalDetectIntentRequest;
  }

  public void
      setOriginalDetectIntentRequest(OriginalDetectIntentRequest originalDetectIntentRequest) {
    this.originalDetectIntentRequest = originalDetectIntentRequest;
  }
  
}
