package org.tiaa.assistant.model.api.google;

import java.util.List;

/**
 * https://dialogflow.com/docs/fulfillment/how-it-works
 * https://github.com/dialogflow/fulfillment-webhook-json/tree/master/requests/v2
 * 
 * @author Admin
 *
 */
public class GoogleWebHookResponse {

  private String fulfillmentText;
  
  private List<FulfillmentMessage> fulfillmentMessages;
  
  private String source;
  
  private Payload payload;
  
  // private Boolean resetContexts = false;

  private List<OutputContext> outputContexts;
  
  private QueryResult followupEventInput;

  public String getFulfillmentText() {
    return fulfillmentText;
  }

  public void setFulfillmentText(String fulfillmentText) {
    this.fulfillmentText = fulfillmentText;
  }

  public List<FulfillmentMessage> getFulfillmentMessages() {
    return fulfillmentMessages;
  }

  public void setFulfillmentMessages(List<FulfillmentMessage> fulfillmentMessages) {
    this.fulfillmentMessages = fulfillmentMessages;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Payload getPayload() {
    return payload;
  }

  public void setPayload(Payload payload) {
    this.payload = payload;
  }

  public List<OutputContext> getOutputContexts() {
    return outputContexts;
  }

  public void setOutputContexts(List<OutputContext> outputContexts) {
    this.outputContexts = outputContexts;
  }

  public QueryResult getFollowupEventInput() {
    return followupEventInput;
  }

  public void setFollowupEventInput(QueryResult followupEventInput) {
    this.followupEventInput = followupEventInput;
  }
  
  /*public Boolean getResetContexts() {
    return resetContexts;
  }
  
  public void setResetContexts(Boolean resetContexts) {
    this.resetContexts = resetContexts;
  }*/
  
}
