package org.tiaa.assistant.model.api.google;

public class Item {

  private SimpleResponse simpleResponse;

  public SimpleResponse getSimpleResponse() {
    return simpleResponse;
  }

  public void setSimpleResponse(SimpleResponse simpleResponse) {
    this.simpleResponse = simpleResponse;
  }
  
}
