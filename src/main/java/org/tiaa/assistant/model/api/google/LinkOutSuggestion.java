package org.tiaa.assistant.model.api.google;

public class LinkOutSuggestion {

  private String destinationName;
  
  private String url;

  public String getDestinationName() {
    return destinationName;
  }

  public void setDestinationName(String destinationName) {
    this.destinationName = destinationName;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
  
  
}
