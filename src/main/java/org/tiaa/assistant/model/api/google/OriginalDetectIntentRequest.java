package org.tiaa.assistant.model.api.google;

public class OriginalDetectIntentRequest {
  
  private String source;
  
  private String version;
  
  private Payload payload;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public Payload getPayload() {
    return payload;
  }

  public void setPayload(Payload payload) {
    this.payload = payload;
  }
  
  
}
