package org.tiaa.assistant.model.api.google;

import java.util.Map;

public class OutputContext {

  private String name;
  
  private Integer lifespanCount;
  
  private Map<String, Object> parameters;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getLifespanCount() {
    return lifespanCount;
  }

  public void setLifespanCount(Integer lifespanCount) {
    this.lifespanCount = lifespanCount;
  }

  public Map<String, Object> getParameters() {
    return parameters;
  }

  public void setParameters(Map<String, Object> parameters) {
    this.parameters = parameters;
  }
  
}
