package org.tiaa.assistant.model.api.google;

import java.util.List;

public class Payload {

  private Boolean isInSandbox;
  
  private Surface surface;
  
  private String requestType;
  
  private List<Input> inputs;
  
  private User user;
  
  private Conversation conversation;
  
  private List<AvailableSurface> availableSurfaces;
  
  ///response
  private Google google;

  public Boolean getIsInSandbox() {
    return isInSandbox;
  }

  public void setIsInSandbox(Boolean isInSandbox) {
    this.isInSandbox = isInSandbox;
  }

  public Surface getSurface() {
    return surface;
  }

  public void setSurface(Surface surface) {
    this.surface = surface;
  }

  public String getRequestType() {
    return requestType;
  }

  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }

  public List<Input> getInputs() {
    return inputs;
  }

  public void setInputs(List<Input> inputs) {
    this.inputs = inputs;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Conversation getConversation() {
    return conversation;
  }

  public void setConversation(Conversation conversation) {
    this.conversation = conversation;
  }

  public List<AvailableSurface> getAvailableSurfaces() {
    return availableSurfaces;
  }

  public void setAvailableSurfaces(List<AvailableSurface> availableSurfaces) {
    this.availableSurfaces = availableSurfaces;
  }

  public Google getGoogle() {
    return google;
  }

  public void setGoogle(Google google) {
    this.google = google;
  }
  
}
