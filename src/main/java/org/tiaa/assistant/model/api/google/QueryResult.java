package org.tiaa.assistant.model.api.google;

import java.util.List;
import java.util.Map;

public class QueryResult {

  private String queryText;
  
  private String action;
  
  private Map<String, Object> parameters;
  
  private Boolean allRequiredParamsPresent;
  
  private String fulfillmentText;
  
  private List<FulfillmentMessage> fulfillmentMessages;
  
  private List<OutputContext> outputContexts;
  
  private Intent intent;
  
  private Float intentDetectionConfidence;
  
  private DiagnosticInfo diagnosticInfo;
  
  private String languageCode;

  public String getQueryText() {
    return queryText;
  }

  public void setQueryText(String queryText) {
    this.queryText = queryText;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public Map<String, Object> getParameters() {
    return parameters;
  }

  public void setParameters(Map<String, Object> parameters) {
    this.parameters = parameters;
  }

  public Boolean getAllRequiredParamsPresent() {
    return allRequiredParamsPresent;
  }

  public void setAllRequiredParamsPresent(Boolean allRequiredParamsPresent) {
    this.allRequiredParamsPresent = allRequiredParamsPresent;
  }

  public String getFulfillmentText() {
    return fulfillmentText;
  }

  public void setFulfillmentText(String fulfillmentText) {
    this.fulfillmentText = fulfillmentText;
  }

  public List<FulfillmentMessage> getFulfillmentMessages() {
    return fulfillmentMessages;
  }

  public void setFulfillmentMessages(List<FulfillmentMessage> fulfillmentMessages) {
    this.fulfillmentMessages = fulfillmentMessages;
  }

  public List<OutputContext> getOutputContexts() {
    return outputContexts;
  }

  public void setOutputContexts(List<OutputContext> outputContexts) {
    this.outputContexts = outputContexts;
  }

  public Intent getIntent() {
    return intent;
  }

  public void setIntent(Intent intent) {
    this.intent = intent;
  }

  public Float getIntentDetectionConfidence() {
    return intentDetectionConfidence;
  }

  public void setIntentDetectionConfidence(Float intentDetectionConfidence) {
    this.intentDetectionConfidence = intentDetectionConfidence;
  }

  public DiagnosticInfo getDiagnosticInfo() {
    return diagnosticInfo;
  }

  public void setDiagnosticInfo(DiagnosticInfo diagnosticInfo) {
    this.diagnosticInfo = diagnosticInfo;
  }

  public String getLanguageCode() {
    return languageCode;
  }

  public void setLanguageCode(String languageCode) {
    this.languageCode = languageCode;
  }
  
}
