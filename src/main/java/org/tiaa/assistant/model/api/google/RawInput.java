package org.tiaa.assistant.model.api.google;

public class RawInput {

  private String query;
  
  private String inputType;

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public String getInputType() {
    return inputType;
  }

  public void setInputType(String inputType) {
    this.inputType = inputType;
  }
  
  
}
