package org.tiaa.assistant.model.api.google;

import java.util.List;

public class RichResponse {

  private List<Item> items;
  
  private List<Suggestion> suggestions;
  
  private LinkOutSuggestion linkOutSuggestion;

  public List<Item> getItems() {
    return items;
  }

  public void setItems(List<Item> items) {
    this.items = items;
  }

  public List<Suggestion> getSuggestions() {
    return suggestions;
  }

  public void setSuggestions(List<Suggestion> suggestions) {
    this.suggestions = suggestions;
  }

  public LinkOutSuggestion getLinkOutSuggestion() {
    return linkOutSuggestion;
  }

  public void setLinkOutSuggestion(LinkOutSuggestion linkOutSuggestion) {
    this.linkOutSuggestion = linkOutSuggestion;
  }
  
}
