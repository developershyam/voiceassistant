
package org.tiaa.assistant.model.api.google;

public class SimpleResponse {

  private String textToSpeech;
  
  private String displayText;
  
  private String ssml;

  public String getTextToSpeech() {
    return textToSpeech;
  }

  public void setTextToSpeech(String textToSpeech) {
    this.textToSpeech = textToSpeech;
  }

  public String getDisplayText() {
    return displayText;
  }

  public void setDisplayText(String displayText) {
    this.displayText = displayText;
  }

  public String getSsml() {
    return ssml;
  }

  public void setSsml(String ssml) {
    this.ssml = ssml;
  }
  
}
