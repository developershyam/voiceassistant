package org.tiaa.assistant.model.api.google;

public class Suggestion {

  private String title;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
  
}
