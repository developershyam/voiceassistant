package org.tiaa.assistant.model.api.google;

import java.util.List;

public class Surface {

  private List<Capability> capabilities;

  public List<Capability> getCapabilities() {
    return capabilities;
  }

  public void setCapabilities(List<Capability> capabilities) {
    this.capabilities = capabilities;
  }
  
}
