package org.tiaa.assistant.model.api.google;

public class SystemIntent {
  
  private String intent;
  
  private DialogSpec dialogSpec;

  public String getIntent() {
    return intent;
  }

  public void setIntent(String intent) {
    this.intent = intent;
  }

  public DialogSpec getDialogSpec() {
    return dialogSpec;
  }

  public void setDialogSpec(DialogSpec dialogSpec) {
    this.dialogSpec = dialogSpec;
  }
  
}
