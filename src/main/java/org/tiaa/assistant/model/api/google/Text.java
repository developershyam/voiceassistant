
package org.tiaa.assistant.model.api.google;

import java.util.List;

public class Text {

  private List<String> text;

  public List<String> getText() {
    return text;
  }

  public void setText(List<String> text) {
    this.text = text;
  }
  
}
