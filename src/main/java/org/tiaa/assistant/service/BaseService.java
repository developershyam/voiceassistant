
package org.tiaa.assistant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import org.tiaa.assistant.util.MessageUtility;

public abstract class BaseService {

  @Autowired
  protected MessageUtility messageUtility;
  
  @Autowired
  protected RestTemplate restTemplate;

}
