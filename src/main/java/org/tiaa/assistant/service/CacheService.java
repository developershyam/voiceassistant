package org.tiaa.assistant.service;

import java.util.Map;

import org.tiaa.assistant.model.UserData;

public interface CacheService {

  UserData updateUserData(String session, Integer age, Long annualIncome, Integer retirementAge);
  
  UserData updateUserData(String session, UserData userData);
  
  UserData getUserData(String session);
  
  Map<String, Long> getActiveSession();
  
  void removeCache(String session);
}
