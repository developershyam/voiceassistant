
package org.tiaa.assistant.service;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.tiaa.assistant.model.UserData;

@Service
public class CacheServiceImpl extends BaseService implements CacheService {

  private static final Logger LOGGER = Logger.getLogger(CacheServiceImpl.class.getName());

  private Map<String, Long> activeSession = new HashMap<>();

  /**
   * This method will update cache.
   * 
   * @param session
   *        user session
   * @param goalName
   *        goal name
   * @param age
   *        current age
   * @return user cache data with session
   */
  @Override
  @CachePut(
      value = "userDataCache",
      key = "#session")
  public UserData updateUserData(String session, Integer age, Long annualIncome, Integer retirementAge) {
    LOGGER.info("Executing updateUserData for session: " + session);
    UserData userData = new UserData(session, age, annualIncome, retirementAge);
    return userData;
  }

  /**
   * This method will update cache.
   * 
   * @param session
   *        user session
   * @param goalName
   *        goal name
   * @param age
   *        current age
   * @return user cache data with session
   */
  @Override
  @CachePut(
      value = "userDataCache",
      key = "#session")
  public UserData updateUserData(String session, UserData userData) {
    LOGGER.info("Executing updateUserData for session: " + session);
    return userData;
  }

  /**
   * This method will get user cached data.
   * 
   * @param session
   *        user session
   */
  @Override
  @Cacheable(
      value = "userDataCache",
      key = "#session")
  @Caching()
  public UserData getUserData(String session) {
    LOGGER.info("Executing getUserData for session: " + session);
    UserData userData = new UserData();
    userData.setSession(session);
    return userData;
  }

  @Override
  @CacheEvict(
      value = "userDataCache",
      key = "#session")
  public void removeCache(String session) {
    LOGGER.info("Executing removeCache for session: " + session);
  }

  public Map<String, Long> getActiveSession() {
    return activeSession;
  }
}
