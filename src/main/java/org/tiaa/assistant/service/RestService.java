package org.tiaa.assistant.service;

import org.tiaa.assistant.model.ActionResponse;
import org.tiaa.assistant.model.InvestmentAdvice;
import org.tiaa.assistant.model.RetrieveProjectionResponse;
import org.tiaa.assistant.model.UserData;

public interface RestService {

  InvestmentAdvice getInvestmentAdvice(UserData userData);
  
  RetrieveProjectionResponse getRetrieveProjections(UserData userData);
  
  ActionResponse getAction(UserData userData);
}
