
package org.tiaa.assistant.service;

import org.springframework.stereotype.Service;
import org.tiaa.assistant.model.ActionRequest;
import org.tiaa.assistant.model.ActionResponse;
import org.tiaa.assistant.model.InvestmentAdvice;
import org.tiaa.assistant.model.InvestmentProfile;
import org.tiaa.assistant.model.RetrieveProjectionRequest;
import org.tiaa.assistant.model.RetrieveProjectionResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.util.AppConstants;
import org.tiaa.assistant.util.WebHookUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RestServiceImpl extends BaseService implements RestService {

  @Override
  public InvestmentAdvice getInvestmentAdvice(UserData userData) {
    InvestmentProfile input = new InvestmentProfile(userData.getAge(), userData.getAnnualIncome());
    InvestmentAdvice investmentAdvice = new InvestmentAdvice();
    try {
      System.out.println("********************INVESTMENT_ADVICE_URL******************************");
      System.out.println(input);
      investmentAdvice = WebHookUtils.getData(restTemplate, AppConstants.INVESTMENT_ADVICE_URL, "POST", input, InvestmentAdvice.class);
      System.out.println(investmentAdvice);
    } catch (Exception e) {
      investmentAdvice.setStatus("ERROR");
    }
    return investmentAdvice;
  }

  @Override
  public RetrieveProjectionResponse getRetrieveProjections(UserData userData) {
    RetrieveProjectionRequest projectionRequest = new RetrieveProjectionRequest();
    projectionRequest.setUserAge(userData.getAge());
    projectionRequest.setGrossAnnualIncome(userData.getAnnualIncome());
    projectionRequest.setRetirementAge(userData.getRetirementAge());
    projectionRequest.setRetirementAssetBalance(userData.getRetirementSavings());
    projectionRequest.setContribution(userData.getAnnualContribution());
    projectionRequest.setRiskLevel(userData.getRiskLevel());
    projectionRequest.setReplacementIncome(userData.getReplacementIncome());
    RetrieveProjectionResponse retrieveProjectionResponse = new RetrieveProjectionResponse();
    try {
      System.out.println("********************RETRIEVE_PROJECTION_URL******************************");
      System.out.println(projectionRequest);
      retrieveProjectionResponse =
          WebHookUtils.getData(restTemplate, AppConstants.RETRIEVE_PROJECTION_URL, "POST", projectionRequest, RetrieveProjectionResponse.class);
      System.out.println(retrieveProjectionResponse);
    } catch (Exception e) {
      retrieveProjectionResponse.setStatus("ERROR");
    }
    return retrieveProjectionResponse;
  }

  @Override
  public ActionResponse getAction(UserData userData) {
    ActionRequest actionRequest = new ActionRequest();
    actionRequest.setUserAge(userData.getAge());
    actionRequest.setGrossAnnualIncome(userData.getAnnualIncome());
    actionRequest.setRetirementAge(userData.getRetirementAge());
    actionRequest.setRetirementAssetBalance(userData.getRetirementSavings());
    actionRequest.setContribution(userData.getAnnualContribution());
    actionRequest.setRiskLevel(userData.getRiskLevel());
    actionRequest.setReplacementIncome(userData.getReplacementIncome());
    ActionResponse actionResponse = new ActionResponse();
    try {
      System.out.println("********************ACTION_URL******************************");
      System.out.println(actionRequest);
      actionResponse = WebHookUtils.getData(restTemplate, AppConstants.ACTION_URL, "POST", actionRequest, ActionResponse.class);
      System.out.println(actionResponse);
    } catch (Exception e) {
      actionResponse.setStatus("ERROR");
    }
    return actionResponse;
  }
}
