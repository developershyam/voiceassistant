
package org.tiaa.assistant.transform;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.api.alexa.AlexaWebHookRequest;
import org.tiaa.assistant.model.api.alexa.AlexaWebHookResponse;
import org.tiaa.assistant.model.api.alexa.Directive;
import org.tiaa.assistant.model.api.alexa.Intent;
import org.tiaa.assistant.model.api.alexa.OutputSpeech;
import org.tiaa.assistant.model.api.alexa.Response;
import org.tiaa.assistant.model.api.alexa.Slot;
import org.tiaa.assistant.util.AlexaIntents;
import org.tiaa.assistant.util.AppUtils;
import org.tiaa.assistant.util.TIAAIntents;

@Component
public class AlexaTransformer {

  public TIAAAssistantRequest getTIAAAssistantRequest(AlexaWebHookRequest alexaWebHookRequest) {

    TIAAAssistantRequest tiaaAssistantRequest = new TIAAAssistantRequest();

    tiaaAssistantRequest.setDeviceName("Alexa");
    tiaaAssistantRequest.setVersion(alexaWebHookRequest.getVersion());
    this.setIntentName(getIntenetName(alexaWebHookRequest), tiaaAssistantRequest);

    tiaaAssistantRequest.setSessionId(alexaWebHookRequest.getSession().getSessionId());
    Intent intent = alexaWebHookRequest.getRequest().getIntent();
    if (intent != null && AppUtils.hasValue(intent.getSlots())) {
      intent.getSlots().entrySet().forEach(slot -> {
        tiaaAssistantRequest.getAttributes().put(slot.getValue().getName(),
            slot.getValue().getValue());
      });
    }
    tiaaAssistantRequest.setIntent(intent);

    return tiaaAssistantRequest;
  }

  private String getIntenetName(AlexaWebHookRequest alexaWebHookRequest) {

    String intenetName = "NoFound";
    if (AlexaIntents.INTENT_TYPE_LAUNCHREQUEST
        .equalsIgnoreCase(alexaWebHookRequest.getRequest().getType())) {
      intenetName = AlexaIntents.DEFAULT_WELCOME_INTENT;
    }
    else if (AlexaIntents.INTENT_TYPE_INTENTREQUEST
        .equalsIgnoreCase(alexaWebHookRequest.getRequest().getType())) {
      intenetName = alexaWebHookRequest.getRequest().getIntent().getName();
    }
    return intenetName;
  }

  private void setIntentName(String intentDisplayName, TIAAAssistantRequest tiaaAssistantRequest) {

    if (AlexaIntents.DEFAULT_WELCOME_INTENT.equalsIgnoreCase(intentDisplayName)) {
      tiaaAssistantRequest.setIntentName(TIAAIntents.DEFAULT_WELCOME_INTENT);
    }
    else if (AlexaIntents.GOAL_SUMMARY.equalsIgnoreCase(intentDisplayName)) {
      tiaaAssistantRequest.setIntentName(TIAAIntents.GOAL_SUMMARY);
    }
    else if (AlexaIntents.RETIREMENT_CHECKUP.equalsIgnoreCase(intentDisplayName)) {
      tiaaAssistantRequest.setIntentName(TIAAIntents.RETIREMENT_CHECKUP);
    }
    else if (AlexaIntents.GOAL_STATUS.equalsIgnoreCase(intentDisplayName)) {
      tiaaAssistantRequest.setIntentName(TIAAIntents.GOAL_STATUS);
    }
  }

  public AlexaWebHookResponse getAlexaWebHookResponse(TIAAAssistantRequest tiaaAssistantRequest,
      TIAAAssistantResponse tiaaAssistantResponse) {

    AlexaWebHookResponse alexaWebHookResponse = new AlexaWebHookResponse();

    alexaWebHookResponse.setVersion(tiaaAssistantRequest.getVersion());

    OutputSpeech outputSpeech = new OutputSpeech();

    if (tiaaAssistantResponse.getResponseType().equalsIgnoreCase("SSML")) {
      setOutputSpeechSSML(outputSpeech, tiaaAssistantResponse.getResponseSpeech(),
          tiaaAssistantResponse.getDisplayText());
    }
    else {
      setOutputSpeech(outputSpeech, tiaaAssistantResponse.getResponseSpeech());
    }
    Response responseWebHook = createResponseForOutputSpeech(outputSpeech);

    alexaWebHookResponse.setResponse(responseWebHook);

    Intent intent = tiaaAssistantRequest.getIntent();
    if (intent != null && AppUtils.hasValue(intent.getSlots())
        && AppUtils.hasValue(tiaaAssistantRequest.getAttributes())) {
      
      for (Map.Entry<String, Object> entry : tiaaAssistantRequest.getAttributes().entrySet()) {
        System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
        if (entry.getValue() != null && AppUtils.hasValue(entry.getValue().toString())) {
          intent.getSlots().put(entry.getKey(),
              new Slot(entry.getKey(), entry.getValue().toString()));
        }
      }
      
      List<Directive> directives = new ArrayList<>();
      if(AppUtils.hasValue(tiaaAssistantResponse.getAskForSlot())) {
        Directive directive = new Directive("Dialog.ElicitSlot", tiaaAssistantResponse.getAskForSlot());
        directive.setUpdatedIntent(intent);
        directives.add(directive);
      }
      responseWebHook.setDirectives(directives);
    }
    return alexaWebHookResponse;
  }

  private Response createResponseForOutputSpeech(OutputSpeech outputSpeech) {

    Response responseWebHook = new Response();
    responseWebHook.setOutputSpeech(outputSpeech);
    responseWebHook.setShouldEndSession(false);

    return responseWebHook;
  }

  private void setOutputSpeech(OutputSpeech outputSpeech, String text) {
    outputSpeech.setType("PlainText");
    outputSpeech.setText(text);
  }

  private void setOutputSpeechSSML(OutputSpeech outputSpeech, String ssml, String displayText) {
    outputSpeech.setType("SSML");
    outputSpeech.setSsml(ssml);
    outputSpeech.setText(displayText);
  }
}
