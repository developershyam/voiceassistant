
package org.tiaa.assistant.transform;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.tiaa.assistant.model.TIAAAssistantRequest;
import org.tiaa.assistant.model.TIAAAssistantResponse;
import org.tiaa.assistant.model.api.google.Google;
import org.tiaa.assistant.model.api.google.GoogleWebHookRequest;
import org.tiaa.assistant.model.api.google.GoogleWebHookResponse;
import org.tiaa.assistant.model.api.google.Item;
import org.tiaa.assistant.model.api.google.Payload;
import org.tiaa.assistant.model.api.google.RichResponse;
import org.tiaa.assistant.model.api.google.SimpleResponse;
import org.tiaa.assistant.util.AppUtils;
import org.tiaa.assistant.util.GoogleIntents;
import org.tiaa.assistant.util.TIAAIntents;

@Component
public class GoogleTransformer {

  public TIAAAssistantRequest getTIAAAssistantRequest(GoogleWebHookRequest googleWebHookRequest) {

    TIAAAssistantRequest tiaaAssistantRequest = new TIAAAssistantRequest();

    tiaaAssistantRequest.setDeviceName("Google");
    this.setIntentName(googleWebHookRequest.getQueryResult().getIntent().getDisplayName(),
        tiaaAssistantRequest);

    tiaaAssistantRequest.setSessionId(googleWebHookRequest.getSession());
    if (AppUtils.hasValue(googleWebHookRequest.getQueryResult().getParameters())) {
      // tiaaAssistantRequest.setAttributes(googleWebHookRequest.getQueryResult().getParameters());
      googleWebHookRequest.getQueryResult().getParameters().entrySet().forEach(entry -> {
        /*if(!entry.getKey().contains(".original")){
          tiaaAssistantRequest.getAttributes().put(entry.getKey(), entry.getValue());
        }*/
        tiaaAssistantRequest.getAttributes().put(entry.getKey(), entry.getValue());
      });
    }
    tiaaAssistantRequest.setQueryResult(googleWebHookRequest.getQueryResult());

    return tiaaAssistantRequest;
  }

  private void setIntentName(String intentDisplayName, TIAAAssistantRequest tiaaAssistantRequest) {

    if (GoogleIntents.DEFAULT_WELCOME_INTENT.equalsIgnoreCase(intentDisplayName)) {
      tiaaAssistantRequest.setIntentName(TIAAIntents.DEFAULT_WELCOME_INTENT);
    }
    else if (GoogleIntents.GOAL_SUMMARY.equalsIgnoreCase(intentDisplayName)) {
      tiaaAssistantRequest.setIntentName(TIAAIntents.GOAL_SUMMARY);
    }
    else if (GoogleIntents.RETIREMENT_CHECKUP.equalsIgnoreCase(intentDisplayName)) {
      tiaaAssistantRequest.setIntentName(TIAAIntents.RETIREMENT_CHECKUP);
    }
    else if (GoogleIntents.GOAL_STATUS.equalsIgnoreCase(intentDisplayName)) {
      tiaaAssistantRequest.setIntentName(TIAAIntents.GOAL_STATUS);
    }
  }

  public GoogleWebHookResponse getGoogleWebHookResponse(TIAAAssistantRequest tiaaAssistantRequest,
      TIAAAssistantResponse tiaaAssistantResponse) {

    GoogleWebHookResponse googleWebHookResponse = new GoogleWebHookResponse();

    SimpleResponse simpleResponse = new SimpleResponse();
    if (tiaaAssistantResponse.getResponseType().equalsIgnoreCase("SSML")) {
      setSimpleResponseSSML(simpleResponse, tiaaAssistantResponse.getResponseSpeech(),
          tiaaAssistantResponse.getDisplayText());
    }
    else {
      setSimpleResponse(simpleResponse, tiaaAssistantResponse.getResponseSpeech(),
          tiaaAssistantResponse.getDisplayText());
    }

    googleWebHookResponse.setPayload(this.createGooglePayloadForSimpleResponse(simpleResponse));

    tiaaAssistantRequest.getQueryResult().getOutputContexts().forEach(context -> {
      if (AppUtils.hasValue(context.getParameters())) {
        // context.getParameters().putAll(tiaaAssistantRequest.getAttributes());
        tiaaAssistantRequest.getAttributes().entrySet().forEach(entry -> {
          context.getParameters().put(entry.getKey(), entry.getValue());
          // context.getParameters().put(entry.getKey()+".original", entry.getValue());
        });
      }
    });
    googleWebHookResponse
        .setOutputContexts(tiaaAssistantRequest.getQueryResult().getOutputContexts());

    return googleWebHookResponse;
  }

  private Payload createGooglePayloadForSimpleResponse(SimpleResponse simpleResponse) {

    List<Item> items = new ArrayList<>();
    Item item = new Item();
    item.setSimpleResponse(simpleResponse);
    items.add(item);

    RichResponse richResponse = new RichResponse();
    richResponse.setItems(items);

    Google google = new Google();
    google.setExpectUserResponse(true);
    google.setRichResponse(richResponse);

    Payload payload = new Payload();
    payload.setGoogle(google);

    return payload;
  }

  private void setSimpleResponse(SimpleResponse simpleResponse, String responseSpeech,
      String displaytext) {
    simpleResponse.setTextToSpeech(responseSpeech);
    simpleResponse.setDisplayText(displaytext);

  }

  private void setSimpleResponseSSML(SimpleResponse simpleResponse, String responseSpeech,
      String displaytext) {
    simpleResponse.setSsml(responseSpeech);
    simpleResponse.setDisplayText(displaytext);
  }
}
