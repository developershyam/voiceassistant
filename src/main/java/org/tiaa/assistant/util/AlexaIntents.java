
package org.tiaa.assistant.util;

public interface AlexaIntents {


  public static final String DEFAULT_WELCOME_INTENT = "Default Welcome Intent";
  
  public static final String INTENT_TYPE_LAUNCHREQUEST = "LaunchRequest";

  public static final String INTENT_TYPE_INTENTREQUEST = "IntentRequest";

  public static final String GOAL_SUMMARY = "GoalSummary";

  public static final String GOAL_STATUS = "GoalStatus";

  public static final String RETIREMENT_CHECKUP = "RetirementCheckup";

}
