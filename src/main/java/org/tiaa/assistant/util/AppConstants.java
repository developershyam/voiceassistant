
package org.tiaa.assistant.util;

public interface AppConstants {

  public static final String DONE = "DONE!!!";

  public static final int PAUSE_400_MILLS = 400;
  public static final int PAUSE_800_MILLS = 400;

  public static final String INVESTMENT_ADVICE_URL = "https://shared.tiaa.org/private/robo/engagement/investmentadvice";

  public static final String RETRIEVE_PROJECTION_URL = "https://shared.tiaa.org/private/robo/engagement/retrieveProjections";
  public static final String ACTION_URL = "https://shared.tiaa.org/private/robo/engagement/investment/getAction";

}
