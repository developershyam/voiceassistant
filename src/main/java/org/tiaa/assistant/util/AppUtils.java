
package org.tiaa.assistant.util;

import java.util.Calendar;
import java.util.Collection;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class AppUtils {

  /**
   * Check if string has value. it should not be null and empty string.
   * 
   * @param value
   *        value
   * @return true if value has string otherwise will return false.
   */
  public static boolean hasValue(String value) {
    return StringUtils.hasText(value);
  }

  /**
   * Check if number type has value greater than 0.
   * 
   * @param number
   *        input number like Integer or Float
   * @return true if number has value otherwise will return false.
   */
  public static boolean hasValue(Number number) {
    if (number != null && number.doubleValue() > 0) {
      return true;
    }
    return false;
  }
  
  public static boolean hasValueInParam(Object obj) {
    if(obj !=null && obj instanceof String && AppUtils.hasValue(obj.toString())) {
      return true;
    } else if(obj !=null && obj instanceof Number && AppUtils.hasValue((Number) obj)) {
      return true;
    }
    return false;
  }

  /**
   * Check if value exist for collection. it should not be null and should contains at at least one
   * object.
   * 
   * @param value
   *        collection
   * @return true if collection contains value otherwise will return false.
   */
  public static boolean hasValue(Collection<?> value) {
    if (value != null && value.size() > 0) {
      return true;
    }
    return false;
  }

  /**
   * Check if value exist for map. it should not be null and should contains at at least one entry.
   * 
   * @param value
   *        map.
   * @return true if map contains entry otherwise will return false.
   */
  public static boolean hasValue(Map<String, ?> value) {
    if (value != null && value.size() > 0) {
      return true;
    }
    return false;
  }

  public static String getTimePhrase() {

    String phrase = "Day";
    Calendar c = Calendar.getInstance();
    int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
    if (timeOfDay >= 0 && timeOfDay < 12) {
      phrase = "Morning";
    }
    else if (timeOfDay >= 12 && timeOfDay < 16) {
      phrase = "Afternoon";
    }
    else if (timeOfDay >= 16 && timeOfDay < 24) {
      phrase = "Evening";
    }
    return phrase;
  }

  public static <T> T parse(String json, Class<T> clazz) {

    if (AppUtils.hasValue(json)) {
      return new Gson().fromJson(json, clazz);
    }
    return null;

  }
  
  public static String getJson(Object obj) {
    
    String json = null;
    try {
      json = new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      // TODO: handle exception
    }
    return json;
  }
  
 
}
