
package org.tiaa.assistant.util;

public interface GoogleIntents {
  
  public static final String DEFAULT_WELCOME_INTENT = "Default Welcome Intent";

  public static final String DEFAULT_FALLBACK_INTENT = "Default Fallback Intent";

  public static final String GOAL_SUMMARY = "Goal Summary";
  
  public static final String GOAL_STATUS = "Goal Status";
  
  public static final String RETIREMENT_CHECKUP = "Retirement Checkup";
}
