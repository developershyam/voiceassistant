
package org.tiaa.assistant.util;

import static org.tiaa.assistant.util.AppConstants.PAUSE_400_MILLS;
import static org.tiaa.assistant.util.AppConstants.PAUSE_800_MILLS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tiaa.assistant.model.ActionResponse;
import org.tiaa.assistant.model.InvestmentAdvice;
import org.tiaa.assistant.model.RetrieveProjectionResponse;
import org.tiaa.assistant.model.UserData;
import org.tiaa.assistant.service.RestService;

@Component
public class RetirementCheckupHelper {

  @Autowired
  protected MessageUtility messageUtility;

  @Autowired
  protected RestService restService;

  protected String getMessage(String id) {
    return messageUtility.getMessage(id);
  }

  protected String getMessage(String id, Object... args) {
    return messageUtility.getMessage(id, args);
  }

  public String getRetirementCheckupOverviewSSML(UserData userData) {

    String responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.overview")).pause(PAUSE_400_MILLS)
        .speak(getMessage("retirmentCheckup.myInfo", userData.getAge(), userData.getAnnualIncome(), userData.getRetirementAge())).pause(PAUSE_400_MILLS)
        .speak(getMessage("retirmentCheckup.askForEditInfo")).pause(PAUSE_800_MILLS).speak(getMessage("retirmentCheckup.askForEditInfo.hint")).build();

    return responseSSML;
  }

  public String getInvestmentAdviceSSML(UserData userData, boolean infoUpdated) {
    String responseSSML = null;
    InvestmentAdvice investmentAdvice = restService.getInvestmentAdvice(userData);
    if (investmentAdvice.getStatus().equalsIgnoreCase("SUCCESS")) {
      long replInc = Double.valueOf(investmentAdvice.getRetrieveIncomeGuidance().getAfterTaxRetirementIncomeGoal() * 0.8).longValue();
      userData.setReplacementIncome(replInc);
    } else {
      userData.setReplacementIncome(2001l);
    }

    if (infoUpdated) {
      responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.myInfoUpdated", userData.getAnnualIncome(), userData.getRetirementAge()))
          .pause(PAUSE_400_MILLS).speak(getMessage("retirmentCheckup.investmentAdvice", userData.getReplacementIncome(), 80)).pause(PAUSE_400_MILLS)
          .speak(getMessage("retirmentCheckup.askForIncludeOtherSavings", SSMLBuilder.create().speakWithSayAs(getMessage("tiaa"), "characters").getAsString()))
          .pause(PAUSE_400_MILLS).speak(getMessage("retirmentCheckup.askForIncludeOtherSavings.hint")).build();
    } else {
      responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.investmentAdvice", userData.getReplacementIncome(), 80)).pause(PAUSE_400_MILLS)
          .speak(getMessage("retirmentCheckup.askForIncludeOtherSavings", SSMLBuilder.create().speakWithSayAs(getMessage("tiaa"), "characters").getAsString()))
          .pause(PAUSE_400_MILLS).speak(getMessage("retirmentCheckup.askForIncludeOtherSavings.hint")).build();
    }
    return responseSSML;
  }

  public String getRetrieveProjectionSSML(UserData userData) {
    String responseSSML = null;
    RetrieveProjectionResponse retrieveProjectionResponse = restService.getRetrieveProjections(userData);
    if (retrieveProjectionResponse.getStatus().equalsIgnoreCase("SUCCESS")) {
      userData.setProjectedIncome(retrieveProjectionResponse.getRetrievedProjections().get("Likely").longValue());
      userData.setTrackingStatus(retrieveProjectionResponse.getTrackingStatus());
    } else {
      userData.setProjectedIncome(1001l);
      userData.setTrackingStatus("Off-Track");
    }
    Long gap = userData.getReplacementIncome() - userData.getProjectedIncome();
    if (gap > 0) {
      responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.thanksForInfo")).pause(PAUSE_400_MILLS)
          .speak(getMessage("retirmentCheckup.projection.incomeWithGap", userData.getProjectedIncome(), gap, userData.getReplacementIncome()))
          .pause(PAUSE_800_MILLS).speak(getMessage("retirmentCheckup.projection.hint")).pause(PAUSE_400_MILLS)
          .speak(getMessage("retirmentCheckup.askForActionPlanToFillGap", SSMLBuilder.create().speakWithSayAs(getMessage("tiaa"), "characters").getAsString()))
          .pause(PAUSE_400_MILLS).speak(getMessage("retirmentCheckup.askForActionPlanToFillGap.hint")).build();
    } else {
      responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.projection.checkupDone")).pause(PAUSE_400_MILLS)
          .speak(getMessage("retirmentCheckup.projection.income", userData.getProjectedIncome(), userData.getReplacementIncome())).pause(PAUSE_800_MILLS)
          .speak(getMessage("retirmentCheckup.contactUs")).build();
    }

    return responseSSML;
  }

  public String getActionSSML(UserData userData) {
    String responseSSML = null;
    ActionResponse actionResponse = restService.getAction(userData);
    if (actionResponse.getStatus().equalsIgnoreCase("SUCCESS")) {
      userData.setActionDifferenceAmount(actionResponse.getDifferenceAmount());
    } else {
      userData.setActionDifferenceAmount(101l);
    }

    responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.projection.checkupDone")).pause(PAUSE_400_MILLS)
        .speak(getMessage("retirmentCheckup.projection.actionGap", userData.getActionDifferenceAmount())).pause(PAUSE_400_MILLS)
        .speak(getMessage("retirmentCheckup.contactUs")).build();
    return responseSSML;
  }

  public String getRetirementCheckupDone(UserData userData) {
    String responseSSML = SSMLBuilder.create().speak(getMessage("retirmentCheckup.projection.checkupDone")).pause(PAUSE_400_MILLS)
        .speak(getMessage("retirmentCheckup.contactUs")).build();
    return responseSSML;
  }
}
