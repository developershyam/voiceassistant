package org.tiaa.assistant.util;

/**
 * This is SSML builder. This class is used to generate SSML from plain text.
 * 
 * <blockquote>
 * 
 * <pre>
 * String ssml = SSMLBuilder.create().speak("This is sample SSML!!!").addBreak("strong").startParagraph()
 *    .speak("This is new Para").endParagraph().build();
 * 
 * </pre>
 * 
 * </blockquote>
 * <p>
 * 
 * @version 1.0
 * @author pareeks
 * 
 *
 */
public class SSMLBuilder {

  private StringBuilder ssmlText;

  /**
   * private constructor, not allowed create instance using this. If you want
   * instance use create method.
   */
  private SSMLBuilder() {

    this.ssmlText = new StringBuilder();
  }

  /**
   * This method will create new instance of SSMLBuilder and return it.
   * 
   * @return will return instance of SSML builder.
   */
  public static SSMLBuilder create() {

    return new SSMLBuilder();
  }

  /**
   * Convert plain text into SSML speech.
   * 
   * @param speech
   *            expected output speech from input text.
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder speak(String speech) {

    ssmlText.append(speech);
    return this;
  }

  /**
   * Convert plain text into SSML speech. Modifies the volume, pitch, and rate
   * of the tagged speech.
   * 
   * @param speech
   *            expected output speech from input text.
   * @param volume
   *            volume value can be silent, x-soft, soft, medium, loud,
   *            x-loud, +ndB, -ndB
   * @param pitch
   *            pitch value can be x-low, low, medium, high, x-high, +n%, -n%
   * @param rate
   *            rate value can be x-slow, slow, medium, fast, x-fast, n%
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder speakWithProsody(String speech, String volume, String pitch, String rate) {

    ssmlText.append("<prosody volume=\"").append(volume).append("\" pitch=\"").append(pitch).append("\" rate=\"")
        .append(rate).append("\">").append(speech).append("</prosody>");
    return this;
  }

  /**
   * Convert plain text into SSML speech.
   * 
   * @param speech
   *            expected output speech from input text.
   * @param level
   *            level can be strong, moderate, none, reduced
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder speakWithEmphasis(String speech, String level) {

    ssmlText.append("<emphasis  level=\"").append(level).append("\">").append(speech).append("</emphasis >");
    return this;
  }

  /**
   * Pronounce the specified word or phrase as a different word or phrase.
   * Specify the pronunciation to substitute with the alias attribute.
   * Indicate that the text in the alias attribute value replaces the
   * contained text for pronunciation.
   * 
   * @param aliasSpeach
   *            expected output speech in place of subText.
   * @param subText
   *            display text.
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder speakWithAlias(String aliasSpeach, String subText) {

    ssmlText.append("<sub alias=\"").append(aliasSpeach).append("\">").append(subText).append("</sub>");
    return this;
  }

  /**
   * Describes how the text should be interpreted. This lets you provide
   * additional context to the text and eliminate any ambiguity on how should
   * render the text.
   * 
   * @param speach
   *            expected output speech text.
   * @param interpretAs
   *            value can be digits, characters, ordinal, cardinal, fraction,
   *            verbatim, spell-out, expletive, unit
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder speakWithSayAs(String speach, String interpretAs) {

    ssmlText.append("<say-as interpret-as=\"").append(interpretAs).append("\">").append(speach).append("</say-as>");
    return this;
  }

  /**
   * Represents a pause in the speech. Set the length of the pause with time
   * attributes in milliseconds.
   * 
   * @param milliSecond
   *            milliseconds for take pause in speech.
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder addBreak(int milliSecond) {

    ssmlText.append("<break time=\"").append(milliSecond).append("ms\"/>");
    return this;
  }

  /**
   * Represents a pause in the speech. Set the length of the pause with time
   * attributes in milliseconds.
   * 
   * @param milliSecond
   *            milliseconds for take pause in speech.
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder pause(int milliSecond) {

    return this.addBreak(milliSecond);
  }
  
  public SSMLBuilder pause(int milliSecond, int whiteSpaceCount) {

    this.addBreak(milliSecond);
    this.whiteSpace(whiteSpaceCount);
    return this;
  }

  /**
   * Represents a pause in the speech. Set the length of the pause with
   * strength attributes.
   * 
   * @param strength
   *            strength for take pause in speech. values can be none, x-weak,
   *            weak, medium, strong, x-strong
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder addBreak(String strength) {

    ssmlText.append("<break strength=\"").append(strength).append("\"/>");
    return this;
  }

  /**
   * Represents a pause in the speech. Set the length of the pause with
   * strength attributes.
   * 
   * @param strength
   *            strength for take pause in speech. values can be none, x-weak,
   *            weak, medium, strong, x-strong
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder pause(String strength) {

    return this.addBreak(strength);
  }

  /**
   * Add <s> in SSML speech text, it will start sentence.
   * 
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder startSentence() {

    ssmlText.append("<s>");
    return this;
  }

  /**
   * Add </s> in SSML speech text, it will end sentence.
   * 
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder endSentence() {

    ssmlText.append("</s>");
    return this;
  }

  /**
   * Add
   * <p>
   * in SSML speech text, it will start paragraph.
   * 
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder startParagraph() {

    ssmlText.append("<p>");
    return this;
  }

  /**
   * Add
   * </p>
   * in SSML speech text, it will end paragraph.
   * 
   * @return Return current SSMLBuilder instance
   */
  public SSMLBuilder endParagraph() {

    ssmlText.append("</p>");
    return this;
  }

  /**
   * This method will generate SSML in string format. All the special effects
   * added or provided while building SSML with calling of appropriate method.
   * 
   * @return Return SSML string from plain text.
   * 
   */
  public String build() {

    System.out.println("<speak>" + ssmlText.toString() + "</speak>");
    return "<speak>" + ssmlText.toString() + "</speak>";
  }
  
  /** This method is return current string formed.
   * This can be used to generate intermittent string can be used later with SSML.
   * @return
   */
  public String getAsString() {

    return ssmlText.toString();
  }
  
  /** Add white space in 
   * @return
   */
  public SSMLBuilder whiteSpace(int count) {

    for (int i = 0; i < count; i++ ){
      ssmlText.append(" ");
    }
    return this;
  }

}