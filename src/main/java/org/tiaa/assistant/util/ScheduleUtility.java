
package org.tiaa.assistant.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.tiaa.assistant.service.CacheService;

@Component
public class ScheduleUtility {

  private static final Logger LOGGER = Logger.getLogger(ScheduleUtility.class.getName());

  private final long TTL = 300000;

  @Autowired
  private CacheService cacheService;

  /**
   * This method will remove old cached session values.
   */
  @Scheduled(
      fixedDelay = TTL)
  public void autoCleanUpCacheService() {
    LOGGER.info("ScheduleUtility: autoCleanUpCacheService");

    Set<String> deleteSessions = new HashSet<>();
    Map<String, Long> activeSessions = cacheService.getActiveSession();
    Set<Entry<String, Long>> activeSessionEntrySet = activeSessions.entrySet();
    if (AppUtils.hasValue(activeSessionEntrySet)) {
      for (Iterator<Entry<String, Long>> iterator = activeSessionEntrySet.iterator(); iterator
          .hasNext();) {
        Entry<String, Long> entry = iterator.next();
        if ((System.currentTimeMillis() - entry.getValue().longValue()) > TTL) {
          deleteSessions.add(entry.getKey());
        }
      }

      activeSessions.keySet().removeAll(deleteSessions);
      if (AppUtils.hasValue(deleteSessions)) {
        for (Iterator<String> iterator = deleteSessions.iterator(); iterator.hasNext();) {
          String session = iterator.next();
          cacheService.removeCache(session);
        }
        LOGGER.info("ScheduleUtility: deleted sessions: " + deleteSessions);
      }
    }
  }
}
