
package org.tiaa.assistant.util;

public interface TIAAIntents {

  public static final String DEFAULT_WELCOME_INTENT = "DefaultWelcomeIntent";

  public static final String GOAL_SUMMARY = "GoalSummary";

  public static final String GOAL_STATUS = "GoalStatus";

  public static final String RETIREMENT_CHECKUP = "RetirementCheckup";

}
