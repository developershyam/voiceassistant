package org.tiaa.assistant.util;

import java.util.Arrays;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.tiaa.assistant.model.api.alexa.Intent;

import com.google.gson.Gson;

public class WebHookUtils {

  /**
   * This method will return object from specific key.
   * 
   * @param params
   *        map of key value pair.
   * @param name
   *        key name in map
   * @param clazz
   *        class type in object is expected.
   * @return
   */
  public static <T> T getParameterValue(Map<String, Object> params, String name, Class<T> clazz) {
    T t = null;
    try {
      if (AppUtils.hasValue(params) && params.get(name) != null) {
        Gson gson = new Gson();
        String jsonStr = gson.toJson(params.get(name));
        if (params.get(name) != null & AppUtils.hasValue(jsonStr)) {
          t = new Gson().fromJson(jsonStr, clazz);
        }
      }
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      t = null;
    }
    return t;
  }

  /**
   * This method will return object from specific key. Used to get slot value in Alxa.
   * 
   * @param intent
   *        Intent
   * @param sloatName
   *        sloatName name in intent
   * @param clazz
   *        class type in object is expected.
   * @return
   */
  public static <T> T getParameterValue(Intent intent, String sloatName, Class<T> clazz) {
    T t = null;
    try {
      if (intent != null && AppUtils.hasValue(intent.getSlots())
          && intent.getSlots().get(sloatName) != null) {
        Gson gson = new Gson();
        String jsonStr = gson.toJson(intent.getSlots().get(sloatName).getValue());
        if (intent.getSlots().get(sloatName).getValue() != null & AppUtils.hasValue(jsonStr)) {
          t = new Gson().fromJson(jsonStr, clazz);
        }
      }
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      t = null;
    }
    return t;
  }

  /**
   * This method will call RestTemplate utility. Fetch data from specified end point and convert to
   * desired object.
   * 
   * @param restTemplate
   *        restTemplate object with configured bean.
   * @param url
   *        target end point
   * @param method
   *        http method type of target end point.
   * @param request
   *        request object
   * @param responseType
   *        type of class in which response needed.
   * @return
   */
  public static <E, T> E getData(RestTemplate restTemplate, String url, String method, T request,
      Class<E> responseType) {
    E response = null;
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    if ("GET".equalsIgnoreCase(method)) {
      response = restTemplate.getForEntity(url, responseType).getBody();
    } else if ("POST".equalsIgnoreCase(method)) {
      response = restTemplate.postForEntity(url, request, responseType).getBody();
    }
    return response;
  }

}